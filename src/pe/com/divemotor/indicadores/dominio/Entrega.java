package pe.com.divemotor.indicadores.dominio;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * Clase de dominio de campos en el csv
 *
 */
public class Entrega implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codigoCentro;
	private String codigoTaller;
	private int mes;
	private int anio;
	private String codigoMecanico;
	private String codigoCliente;
	private String hora;
	private String placa;
	private int numeroOrdenTrabajo;
	private String descripcionCentro;
	private String descripcionTaller;
	private String nombreAsesor;
	private String nombreCliente;
	private BigDecimal porcentajeEntrega;
	private String codigoColorEntrega;
	private int totalPaginas;

	public String getCodigoCentro() {
		return codigoCentro;
	}

	public void setCodigoCentro(String codigoCentro) {
		this.codigoCentro = codigoCentro;
	}

	public String getCodigoTaller() {
		return codigoTaller;
	}

	public void setCodigoTaller(String codigoTaller) {
		this.codigoTaller = codigoTaller;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public String getCodigoMecanico() {
		return codigoMecanico;
	}

	public void setCodigoMecanico(String codigoMecanico) {
		this.codigoMecanico = codigoMecanico;
	}

	public String getCodigoCliente() {
		return codigoCliente;
	}

	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public int getNumeroOrdenTrabajo() {
		return numeroOrdenTrabajo;
	}

	public void setNumeroOrdenTrabajo(int numeroOrdenTrabajo) {
		this.numeroOrdenTrabajo = numeroOrdenTrabajo;
	}

	public String getDescripcionCentro() {
		return descripcionCentro;
	}

	public void setDescripcionCentro(String descripcionCentro) {
		this.descripcionCentro = descripcionCentro;
	}

	public String getDescripcionTaller() {
		return descripcionTaller;
	}

	public void setDescripcionTaller(String descripcionTaller) {
		this.descripcionTaller = descripcionTaller;
	}

	public String getNombreAsesor() {
		return nombreAsesor;
	}

	public void setNombreAsesor(String nombreAsesor) {
		this.nombreAsesor = nombreAsesor;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public BigDecimal getPorcentajeEntrega() {
		return porcentajeEntrega;
	}

	public void setPorcentajeEntrega(BigDecimal porcentajeEntrega) {
		this.porcentajeEntrega = porcentajeEntrega;
	}

	public String getCodigoColorEntrega() {
		return codigoColorEntrega;
	}

	public void setCodigoColorEntrega(String codigoColorEntrega) {
		this.codigoColorEntrega = codigoColorEntrega;
	}

	public int getTotalPaginas() {
		return totalPaginas;
	}

	public void setTotalPaginas(int totalPaginas) {
		this.totalPaginas = totalPaginas;
	}

}
