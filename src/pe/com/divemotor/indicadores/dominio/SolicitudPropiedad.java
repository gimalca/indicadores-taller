package pe.com.divemotor.indicadores.dominio;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * Clase de dominio para la solicitud de actualizar propiedades 
 *
 */
public class SolicitudPropiedad implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String contrasena;
	private int idSistema;
	private String loginUsuario;
	private List<Propiedad> variables;

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public int getIdSistema() {
		return idSistema;
	}

	public void setIdSistema(int idSistema) {
		this.idSistema = idSistema;
	}

	public String getLoginUsuario() {
		return loginUsuario;
	}

	public void setLoginUsuario(String loginUsuario) {
		this.loginUsuario = loginUsuario;
	}

	public List<Propiedad> getVariables() {
		return variables;
	}

	public void setVariables(List<Propiedad> variables) {
		this.variables = variables;
	}

}
