package pe.com.divemotor.indicadores.dominio;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * Clase de dominio de campos en el csv 
 *
 */
public class Factura implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codigoCentro;
	private String codigoTaller;
	private int mes;
	private int anio;
	private String codigoMoneda;
	private String descripcionCentro;
	private String descripcionTaller;
	private String monto;
	private BigDecimal porcentajeMeta;
	private String codigoColor;
	private int ordenesAbiertas;
	private BigDecimal porcentajeProductividad;
	private String codigoColorProductividad;
	private BigDecimal porcentajeEficiencia;
	private String codigoColorEficiencia;
	private BigDecimal porcentajeEfectividad;
	private String codigoColorEfectividad;
	private String montoPendienteFacturar;

	public String getCodigoCentro() {
		return codigoCentro;
	}

	public void setCodigoCentro(String codigoCentro) {
		this.codigoCentro = codigoCentro;
	}

	public String getCodigoTaller() {
		return codigoTaller;
	}

	public void setCodigoTaller(String codigoTaller) {
		this.codigoTaller = codigoTaller;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public String getCodigoMoneda() {
		return codigoMoneda;
	}

	public void setCodigoMoneda(String codigoMoneda) {
		this.codigoMoneda = codigoMoneda;
	}

	public String getDescripcionCentro() {
		return descripcionCentro;
	}

	public void setDescripcionCentro(String descripcionCentro) {
		this.descripcionCentro = descripcionCentro;
	}

	public String getDescripcionTaller() {
		return descripcionTaller;
	}

	public void setDescripcionTaller(String descripcionTaller) {
		this.descripcionTaller = descripcionTaller;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public BigDecimal getPorcentajeMeta() {
		return porcentajeMeta;
	}

	public void setPorcentajeMeta(BigDecimal porcentajeMeta) {
		this.porcentajeMeta = porcentajeMeta;
	}

	public int getOrdenesAbiertas() {
		return ordenesAbiertas;
	}

	public void setOrdenesAbiertas(int ordenesAbiertas) {
		this.ordenesAbiertas = ordenesAbiertas;
	}

	public BigDecimal getPorcentajeProductividad() {
		return porcentajeProductividad;
	}

	public void setPorcentajeProductividad(BigDecimal porcentajeProductividad) {
		this.porcentajeProductividad = porcentajeProductividad;
	}

	public String getCodigoColor() {
		return codigoColor;
	}

	public void setCodigoColor(String codigoColor) {
		this.codigoColor = codigoColor;
	}

	public String getCodigoColorProductividad() {
		return codigoColorProductividad;
	}

	public void setCodigoColorProductividad(String codigoColorProductividad) {
		this.codigoColorProductividad = codigoColorProductividad;
	}

	public String getCodigoColorEficiencia() {
		return codigoColorEficiencia;
	}

	public void setCodigoColorEficiencia(String codigoColorEficiencia) {
		this.codigoColorEficiencia = codigoColorEficiencia;
	}

	public BigDecimal getPorcentajeEficiencia() {
		return porcentajeEficiencia;
	}

	public void setPorcentajeEficiencia(BigDecimal porcentajeEficiencia) {
		this.porcentajeEficiencia = porcentajeEficiencia;
	}

	public BigDecimal getPorcentajeEfectividad() {
		return porcentajeEfectividad;
	}

	public void setPorcentajeEfectividad(BigDecimal porcentajeEfectividad) {
		this.porcentajeEfectividad = porcentajeEfectividad;
	}

	public String getCodigoColorEfectividad() {
		return codigoColorEfectividad;
	}

	public void setCodigoColorEfectividad(String codigoColorEfectividad) {
		this.codigoColorEfectividad = codigoColorEfectividad;
	}

	public String getMontoPendienteFacturar() {
		return montoPendienteFacturar;
	}

	public void setMontoPendienteFacturar(String montoPendienteFacturar) {
		this.montoPendienteFacturar = montoPendienteFacturar;
	}

}
