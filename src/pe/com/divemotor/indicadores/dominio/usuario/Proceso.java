package pe.com.divemotor.indicadores.dominio.usuario;

import java.io.Serializable;

public class Proceso implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idProceso;
	private String nombre;
	private boolean esRestrictivo;
	private String esRestrictivoString;

	public int getIdProceso() {
		return idProceso;
	}

	public void setIdProceso(int idProceso) {
		this.idProceso = idProceso;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isEsRestrictivo() {
		return esRestrictivo;
	}

	public void setEsRestrictivo(boolean esRestrictivo) {
		this.esRestrictivo = esRestrictivo;
	}

	public String getEsRestrictivoString() {
		return esRestrictivoString;
	}

	public void setEsRestrictivoString(String esRestrictivoString) {
		this.esRestrictivoString = esRestrictivoString;
	}

}
