package pe.com.divemotor.indicadores.dominio.usuario;

import java.io.Serializable;
import java.util.List;

public class HijoOpcion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean esPadre;
	private String esPadreString;
	private String nombre;
	private String url;
	private int idOpcionPadre;
	private int nroOrden;
	private List<HijoOpcion> hijoOpcionList;
	private int idSistema;
	private int idOpcion;

	public boolean isEsPadre() {
		return esPadre;
	}

	public void setEsPadre(boolean esPadre) {
		this.esPadre = esPadre;
	}

	public String getEsPadreString() {
		return esPadreString;
	}

	public void setEsPadreString(String esPadreString) {
		this.esPadreString = esPadreString;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getIdOpcionPadre() {
		return idOpcionPadre;
	}

	public void setIdOpcionPadre(int idOpcionPadre) {
		this.idOpcionPadre = idOpcionPadre;
	}

	public int getNroOrden() {
		return nroOrden;
	}

	public void setNroOrden(int nroOrden) {
		this.nroOrden = nroOrden;
	}

	public List<HijoOpcion> getHijoOpcionList() {
		return hijoOpcionList;
	}

	public void setHijoOpcionList(List<HijoOpcion> hijoOpcionList) {
		this.hijoOpcionList = hijoOpcionList;
	}

	public int getIdSistema() {
		return idSistema;
	}

	public void setIdSistema(int idSistema) {
		this.idSistema = idSistema;
	}

	public int getIdOpcion() {
		return idOpcion;
	}

	public void setIdOpcion(int idOpcion) {
		this.idOpcion = idOpcion;
	}
}
