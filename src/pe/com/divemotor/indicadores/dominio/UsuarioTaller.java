package pe.com.divemotor.indicadores.dominio;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * Clase de dominio de busqueda de usuarios para talleres 
 *
 */
public class UsuarioTaller implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int idUsuario;
	private String apellido;
	private String nombre;
	private String correo;
	private String login;
	private String perfil;
	private int idSistema;
	private String nombreSistema;
	private List<Taller> talleres;

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public int getIdSistema() {
		return idSistema;
	}

	public void setIdSistema(int idSistema) {
		this.idSistema = idSistema;
	}

	public String getNombreSistema() {
		return nombreSistema;
	}

	public void setNombreSistema(String nombreSistema) {
		this.nombreSistema = nombreSistema;
	}

	public List<Taller> getTalleres() {
		return talleres;
	}

	public void setTalleres(List<Taller> talleres) {
		this.talleres = talleres;
	}

}
