package pe.com.divemotor.indicadores.excepcion;

import pe.com.divemotor.indicadores.util.CodigoEstatus;
import pe.com.divemotor.indicadores.util.Traza;

/**
 * 
 * Clase para definir la excepcion
 *
 */
public class IndicadorException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final Traza traza;
	private final CodigoEstatus status;

	public static final String ERROR_EN_SOLICITUD = "Error de sintaxis, una o mas datos esta en null, vacio";
	public static final String ERROR_PROPIEDAD_NULA_VACIA = "Propiedad {0} es null, vacio: {1}";

	public Traza getTraza() {
		return this.traza;
	}

	public CodigoEstatus getStatus() {
		return this.status;
	}

	protected IndicadorException(CodigoEstatus status, String message) {
		super(message);
		this.traza = Traza.getInstancia();
		this.status = status;
	}

	protected IndicadorException(CodigoEstatus status, Throwable cause) {
		super(cause);
		this.traza = Traza.getInstancia();
		this.status = status;
	}

	protected IndicadorException(CodigoEstatus status, String message, Throwable origen) {
		super(message, origen);
		this.traza = Traza.getInstancia();
		this.status = status;
	}

	public static IndicadorException getInstanciaDesdeMensaje(CodigoEstatus estatus, String message) {
		return new IndicadorException(estatus, message);
	}

	public static IndicadorException getInstanciaDesdeOrigen(CodigoEstatus status, Throwable origin) {
		return new IndicadorException(status, origin);
	}

	public static IndicadorException getInstanciaDesdeMensajeOrigen(CodigoEstatus status, String message,
			Throwable origin) {
		return new IndicadorException(status, message, origin);
	}

}
