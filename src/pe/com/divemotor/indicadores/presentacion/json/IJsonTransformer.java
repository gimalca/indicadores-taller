package pe.com.divemotor.indicadores.presentacion.json;

import java.io.BufferedReader;

/**
 * 
 * Definicion de metodos para manejar los JSON
 *
 */
public interface IJsonTransformer {

	String toJson(Object data);

	Object fromJson(String json, Class clazz);
	
	Object extraer(BufferedReader br);

}
