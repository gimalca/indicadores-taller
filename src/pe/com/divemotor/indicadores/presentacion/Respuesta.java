package pe.com.divemotor.indicadores.presentacion;

import pe.com.divemotor.indicadores.excepcion.IndicadorException;
import pe.com.divemotor.indicadores.util.CodigoEstatus;
import pe.com.divemotor.indicadores.util.Traza;

/**
 * 
 * Clase de templates de respuesta de los servicios
 *
 * @param <T>
 */
public class Respuesta<T> {
	private final int codigo;
	private final String mensaje;
	private final T dato;
	private final Traza traza;

	public int getCodigo() {
		return codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public T getDato() {
		return dato;
	}

	public Traza getTraza() {
		return traza;
	}

	public Respuesta(CodigoEstatus codigo, T dato) {
		this.codigo = codigo.getCodigo();
		this.mensaje = codigo.toString();
		this.traza = Traza.getInstancia();
		this.dato = dato;
	}

	public Respuesta(IndicadorException excepcion) {
		this.codigo = excepcion.getStatus().getCodigo();
		this.mensaje = excepcion.getStatus().toString();
		this.traza = excepcion.getTraza();
		this.dato = null;
	}
}
