package pe.com.divemotor.indicadores.api.v1;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.com.divemotor.indicadores.dominio.Entrega;
import pe.com.divemotor.indicadores.dominio.Factura;
import pe.com.divemotor.indicadores.dominio.Menu;
import pe.com.divemotor.indicadores.dominio.Tecnico;
import pe.com.divemotor.indicadores.dominio.usuario.SolicitudLogueo;
import pe.com.divemotor.indicadores.excepcion.IndicadorException;
import pe.com.divemotor.indicadores.file.ILeerArchivo;
import pe.com.divemotor.indicadores.file.implementacion.LeerArchivoImplEntrega;
import pe.com.divemotor.indicadores.file.implementacion.LeerArchivoImplTecnico;
import pe.com.divemotor.indicadores.helper.IndicadorHelper;
import pe.com.divemotor.indicadores.persistencia.IIndicadorDAO;
import pe.com.divemotor.indicadores.presentacion.Respuesta;
import pe.com.divemotor.indicadores.presentacion.RespuestaLogueo;
import pe.com.divemotor.indicadores.presentacion.Resultado;
import pe.com.divemotor.indicadores.presentacion.json.IJsonTransformer;
import pe.com.divemotor.indicadores.puente.UsuarioCliente;
import pe.com.divemotor.indicadores.util.CodigoEstatus;
import pe.com.divemotor.indicadores.util.Constantes;

/**
 * 
 * Clase de los endpoints para indicadores
 *
 */
@RestController
public class IndicadorController {

	@Autowired
	private IIndicadorDAO indicadorDAO;

	@Autowired
	private ILeerArchivo<Factura> archivo;
	private LeerArchivoImplTecnico archivoTecnico;
	private LeerArchivoImplEntrega archivoEntrega;

	@Autowired
	private IJsonTransformer jsonTransformer;

	@Autowired
	private UsuarioCliente usuarioCliente;

	/**
	 * Endpoint tipo POST que retorna las facturas del archivo csv
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @param jsonIn
	 */
	@RequestMapping(value = "/facturas", method = RequestMethod.POST, consumes = Constantes.APPLICATION_JSON, produces = Constantes.APPLICATION_JSON)
	public void obtenerFacturas(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			@RequestBody String jsonSolicitud) {
		Respuesta<Resultado<Factura>> respuesta = null;
		Resultado<Factura> resultado = new Resultado<Factura>();
		String jsonSalida = null;
		RespuestaLogueo respuestaLogueo = null;
		try {
			SolicitudLogueo solicitudBody = (SolicitudLogueo) jsonTransformer.fromJson(jsonSolicitud,
					SolicitudLogueo.class);
			solicitudBody.validar();
			respuestaLogueo = usuarioCliente.autenticarMock(indicadorDAO, solicitudBody,
					solicitudBody.getLoginUsuario(), indicadorDAO.obtenerPropiedad(Constantes.TOKEN_SERVICIO_USUARIO));
			if (respuestaLogueo != null) {
				if (Constantes.CONF_FTP_ACTIVO.equals(indicadorDAO.obtenerPropiedad(Constantes.CONF_FTP_ACTIVADO))) {
					resultado.setListado(archivo.leerFTP(
							indicadorDAO.obtenerPropiedad(Constantes.CONF_URL_ARCHIVOS)
									+ indicadorDAO.obtenerPropiedad(Constantes.CONF_NOMBRE_ARCHIVO_FACTURACION),
							indicadorDAO, "", respuestaLogueo.getUsuario().getIdUsuario()));
				} else {
					resultado.setListado(archivo.leer(
							indicadorDAO.obtenerPropiedad(Constantes.CONF_URL_ARCHIVOS)
									+ indicadorDAO.obtenerPropiedad(Constantes.CONF_NOMBRE_ARCHIVO_FACTURACION),
							indicadorDAO, "", respuestaLogueo.getUsuario().getIdUsuario()));
				}
				resultado.setValido(true);
				respuesta = new Respuesta<Resultado<Factura>>(CodigoEstatus.OK, resultado);
			}else{
				resultado.setListado(null);
				resultado.setValido(false);
				respuesta = new Respuesta<Resultado<Factura>>(CodigoEstatus.OK, resultado);
			}
			jsonSalida = jsonTransformer.toJson(respuesta);
			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			httpServletResponse.getWriter().println(jsonSalida);

		} catch (IndicadorException ex) {
			respuesta = new Respuesta<Resultado<Factura>>(ex);
			jsonSalida = jsonTransformer.toJson(respuesta);

			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			try {
				httpServletResponse.getWriter().println(jsonSalida);
			} catch (IOException ex1) {
				Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex1);
			}

		} catch (Exception ex) {
			httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	/**
	 * Endpoint tipo POST que retorna el menu dependiendo del perfil
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @param jsonSolicitud
	 */

	@RequestMapping(value = "/menus", method = RequestMethod.POST, consumes = Constantes.APPLICATION_JSON, produces = Constantes.APPLICATION_JSON)
	public void obtenerMenus(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			@RequestBody String jsonSolicitud) {
		Respuesta<Resultado<Menu>> respuesta = null;
		Resultado<Menu> resultado = new Resultado<Menu>();
		String jsonSalida;
		List<Menu> listaMenu = null;
		RespuestaLogueo respuestaLogueo = null;
		try {
			SolicitudLogueo solicitudBody = (SolicitudLogueo) jsonTransformer.fromJson(jsonSolicitud,
					SolicitudLogueo.class);
			solicitudBody.validar();
			respuestaLogueo = usuarioCliente.autenticarMock(indicadorDAO, solicitudBody,
					solicitudBody.getLoginUsuario(), indicadorDAO.obtenerPropiedad(Constantes.TOKEN_SERVICIO_USUARIO));
			for (Integer idPerfil : respuestaLogueo.getPerfilList()) {
				listaMenu = indicadorDAO.obtenerMenu(idPerfil);
				if (!listaMenu.isEmpty()) {
					break;
				}
			}
			resultado.setListado(listaMenu);
			resultado.setValido(true);
			respuesta = new Respuesta<Resultado<Menu>>(CodigoEstatus.OK, resultado);
			jsonSalida = jsonTransformer.toJson(respuesta);
			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			httpServletResponse.getWriter().println(jsonSalida);

		} catch (IndicadorException ex) {
			respuesta = new Respuesta<Resultado<Menu>>(ex);
			jsonSalida = jsonTransformer.toJson(respuesta);

			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			try {
				httpServletResponse.getWriter().println(jsonSalida);
			} catch (IOException ex1) {
				Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex1);
			}

		} catch (Exception ex) {
			httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	/**
	 * Endpoint tipo POST que retorna los tecnicos del archivo csv por pagina
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @param codigoTaller
	 * @param id
	 */

	@RequestMapping(value = "/tecnicos/{codigoTaller}/{id}", method = RequestMethod.GET, produces = Constantes.APPLICATION_JSON)
	public void obtenerTecnicos(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			@PathVariable("codigoTaller") String codigoTaller, @PathVariable("id") int id) {
		Respuesta<Resultado<Tecnico>> respuesta = null;
		String jsonSalida = "";
		archivoTecnico = new LeerArchivoImplTecnico();
		IndicadorHelper ih = new IndicadorHelper();
		try {
			Resultado<Tecnico> resultado = new Resultado<Tecnico>();
			resultado.setListado(null);
			resultado.setValido(false);
			respuesta = new Respuesta<Resultado<Tecnico>>(CodigoEstatus.OK, resultado);
			String jsonTecnicos = jsonTransformer.toJson(respuesta);
			if (Constantes.CONF_FTP_ACTIVO.equals(indicadorDAO.obtenerPropiedad(Constantes.CONF_FTP_ACTIVADO))) {
				List<List<Tecnico>> listaTecnicos = ih.obtenerTecnicosFTP(indicadorDAO, archivoTecnico, codigoTaller);
				if (id < listaTecnicos.size()) {
					jsonTecnicos = ih.obtenerResultadoFTP(listaTecnicos.get(id), jsonTransformer);
				}
			} else {
				List<List<Tecnico>> listaTecnicos = ih.obtenerTecnicos(indicadorDAO, archivoTecnico, codigoTaller);
				if (id < listaTecnicos.size()) {
					jsonTecnicos = ih.obtenerResultado(listaTecnicos.get(id), jsonTransformer);
				}
			}
			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			httpServletResponse.getWriter().println(jsonTecnicos);
		} catch (IndicadorException ex) {
			respuesta = new Respuesta<Resultado<Tecnico>>(ex);
			jsonSalida = jsonTransformer.toJson(respuesta);

			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			try {
				httpServletResponse.getWriter().println(jsonSalida);
			} catch (IOException ex1) {
				Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex1);
			}

		} catch (Exception ex) {
			httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	/**
	 * Endpoint tipo POST que retorna las entregas del archivo csv por pagina
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @param codigoTaller
	 * @param id
	 */

	@RequestMapping(value = "/entregas/{codigoTaller}/{id}", method = RequestMethod.GET, produces = Constantes.APPLICATION_JSON)
	public void obtenerEntregas(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			@PathVariable("codigoTaller") String codigoTaller, @PathVariable("id") int id) {
		Respuesta<Resultado<Entrega>> respuesta = null;
		String jsonSalida = "";
		archivoEntrega = new LeerArchivoImplEntrega();
		IndicadorHelper ih = new IndicadorHelper();
		try {
			Resultado<Entrega> resultado = new Resultado<Entrega>();
			resultado.setListado(null);
			resultado.setValido(false);
			respuesta = new Respuesta<Resultado<Entrega>>(CodigoEstatus.OK, resultado);
			String jsonEntregas = jsonTransformer.toJson(respuesta);
			if (Constantes.CONF_FTP_ACTIVO.equals(indicadorDAO.obtenerPropiedad(Constantes.CONF_FTP_ACTIVADO))) {
				List<List<Entrega>> listaEntregas = ih.obtenerEntregasFTP(indicadorDAO, archivoEntrega, codigoTaller);
				if (id < listaEntregas.size()) {
					jsonEntregas = ih.obtenerResultadoEntregaFTP(listaEntregas.get(id), jsonTransformer);
				}
			} else {
				List<List<Entrega>> listaEntregas = ih.obtenerEntregas(indicadorDAO, archivoEntrega, codigoTaller);
				if (id < listaEntregas.size()) {
					jsonEntregas = ih.obtenerResultadoEntregaFTP(listaEntregas.get(id), jsonTransformer);
				}
			}
			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			httpServletResponse.getWriter().println(jsonEntregas);

		} catch (IndicadorException ex) {
			respuesta = new Respuesta<Resultado<Entrega>>(ex);
			jsonSalida = jsonTransformer.toJson(respuesta);
			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			try {
				httpServletResponse.getWriter().println(jsonSalida);
			} catch (IOException ex1) {
				Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex1);
			}

		} catch (Exception ex) {
			httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
