package pe.com.divemotor.indicadores.api.v1;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.com.divemotor.indicadores.dominio.SolicitudUsuarioTaller;
import pe.com.divemotor.indicadores.dominio.Taller;
import pe.com.divemotor.indicadores.dominio.usuario.SolicitudLogueo;
import pe.com.divemotor.indicadores.excepcion.IndicadorException;
import pe.com.divemotor.indicadores.persistencia.IIndicadorDAO;
import pe.com.divemotor.indicadores.presentacion.Respuesta;
import pe.com.divemotor.indicadores.presentacion.RespuestaLogueo;
import pe.com.divemotor.indicadores.presentacion.Resultado;
import pe.com.divemotor.indicadores.presentacion.json.IJsonTransformer;
import pe.com.divemotor.indicadores.puente.UsuarioCliente;
import pe.com.divemotor.indicadores.util.CodigoEstatus;
import pe.com.divemotor.indicadores.util.Constantes;

/**
 * 
 * Clase de los endpoints de talleres
 *
 */
@RestController
public class TallerController {

	@Autowired
	private IIndicadorDAO indicadorDAO;

	@Autowired
	private IJsonTransformer jsonTransformer;
	
	@Autowired
	private UsuarioCliente usuarioCliente;

	/**
	 * Endpoint tipo POST que retorna las lista de talleres disponibles para asignar
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @param jsonSolicitud
	 */
	@RequestMapping(value = "/talleres", method = RequestMethod.POST, consumes = Constantes.APPLICATION_JSON, produces = Constantes.APPLICATION_JSON)
	public void talleres(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			@RequestBody String jsonSolicitud) {
		Respuesta<Resultado<Taller>> respuesta = null;
		Resultado<Taller> resultado = new Resultado<Taller>();
		String jsonSalida;
		RespuestaLogueo respuestaLogueo = null;
		try {
			SolicitudLogueo solicitudBody = (SolicitudLogueo) jsonTransformer.fromJson(jsonSolicitud,
					SolicitudLogueo.class);
			solicitudBody.validar();
			respuestaLogueo = usuarioCliente.autenticarMock(indicadorDAO, solicitudBody,
					solicitudBody.getLoginUsuario(), indicadorDAO.obtenerPropiedad(Constantes.TOKEN_SERVICIO_USUARIO));
			if(respuestaLogueo != null){
				resultado.setListado(indicadorDAO.listarTaller());
				resultado.setValido(true);
			}else{
				resultado.setListado(null);
				resultado.setValido(false);
			}				
			respuesta = new Respuesta<Resultado<Taller>>(CodigoEstatus.OK, resultado);
			jsonSalida = jsonTransformer.toJson(respuesta);
			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			httpServletResponse.getWriter().println(jsonSalida);
		} catch (IndicadorException ex) {
			respuesta = new Respuesta<Resultado<Taller>>(ex);
			jsonSalida = jsonTransformer.toJson(respuesta);

			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			try {
				httpServletResponse.getWriter().println(jsonSalida);
			} catch (IOException ex1) {
				Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex1);
			}

		} catch (Exception ex) {
			httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	/**
	 * Endpoint tipo PUT que actualiza las lista de talleres asociados a un usuario
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @param jsonSolicitud
	 * @param id
	 */
	@RequestMapping(value = "/taller/{id}", method = RequestMethod.PUT, consumes = Constantes.APPLICATION_JSON, produces = Constantes.APPLICATION_JSON)
	public void update(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			@RequestBody String jsonSolicitud, @PathVariable("id") int id) {
		Respuesta<SolicitudUsuarioTaller> respuesta;
		String jsonSalida;
		try {
			SolicitudUsuarioTaller usuarioTaller = (SolicitudUsuarioTaller) jsonTransformer.fromJson(jsonSolicitud, SolicitudUsuarioTaller.class);
			indicadorDAO.updateUsuarioTaller(id, usuarioTaller);
			respuesta = new Respuesta<SolicitudUsuarioTaller>(CodigoEstatus.OK, usuarioTaller);
			jsonSalida = jsonTransformer.toJson(respuesta);

			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			httpServletResponse.setContentType("application/json; charset=UTF-8");
			httpServletResponse.getWriter().println(jsonSalida);

		} catch (IndicadorException ex) {
			respuesta = new Respuesta<SolicitudUsuarioTaller>(ex);
			jsonSalida = jsonTransformer.toJson(respuesta);

			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			try {
				httpServletResponse.getWriter().println(jsonSalida);
			} catch (IOException ex1) {
				Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex1);
			}

		} catch (Exception ex) {
			httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
