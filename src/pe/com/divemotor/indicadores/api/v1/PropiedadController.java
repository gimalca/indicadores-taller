package pe.com.divemotor.indicadores.api.v1;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.com.divemotor.indicadores.dominio.Propiedad;
import pe.com.divemotor.indicadores.dominio.SolicitudPropiedad;
import pe.com.divemotor.indicadores.dominio.usuario.SolicitudLogueo;
import pe.com.divemotor.indicadores.excepcion.IndicadorException;
import pe.com.divemotor.indicadores.persistencia.IIndicadorDAO;
import pe.com.divemotor.indicadores.presentacion.Informacion;
import pe.com.divemotor.indicadores.presentacion.Respuesta;
import pe.com.divemotor.indicadores.presentacion.RespuestaLogueo;
import pe.com.divemotor.indicadores.presentacion.Resultado;
import pe.com.divemotor.indicadores.presentacion.json.IJsonTransformer;
import pe.com.divemotor.indicadores.puente.UsuarioCliente;
import pe.com.divemotor.indicadores.util.CodigoEstatus;
import pe.com.divemotor.indicadores.util.Constantes;

/**
 * 
 * Clase de los endpoints de propiedades
 *
 */
@RestController
public class PropiedadController {

	@Autowired
	private IIndicadorDAO indicadorDAO;

	@Autowired
	private IJsonTransformer jsonTransformer;

	@Autowired
	private UsuarioCliente usuarioConsume;

	/**
	 * Endpoint tipo POST que retorna las lista de propiedades de configuracion
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @param jsonSolicitud
	 */
	@RequestMapping(value = "/configuraciones", method = RequestMethod.POST, consumes = Constantes.APPLICATION_JSON, produces = Constantes.APPLICATION_JSON)
	public void configuraciones(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			@RequestBody String jsonSolicitud) {
		Respuesta<Resultado<Propiedad>> respuesta = null;
		Resultado<Propiedad> resultado = new Resultado<Propiedad>();
		String jsonSalida;
		RespuestaLogueo respuestaLogueo = null;
		try {
			SolicitudLogueo solicitudBody = (SolicitudLogueo) jsonTransformer.fromJson(jsonSolicitud,
					SolicitudLogueo.class);
			solicitudBody.validar();
			respuestaLogueo = usuarioConsume.autenticarMock(indicadorDAO, solicitudBody,
					solicitudBody.getLoginUsuario(), indicadorDAO.obtenerPropiedad(Constantes.TOKEN_SERVICIO_USUARIO));
			if (respuestaLogueo != null) {
				resultado.setListado(indicadorDAO.listar());
				resultado.setValido(true);
			} else {
				resultado.setListado(null);
				resultado.setValido(false);
			}
			respuesta = new Respuesta<Resultado<Propiedad>>(CodigoEstatus.OK, resultado);
			jsonSalida = jsonTransformer.toJson(respuesta);
			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			httpServletResponse.getWriter().println(jsonSalida);
		} catch (IndicadorException ex) {
			respuesta = new Respuesta<Resultado<Propiedad>>(ex);
			jsonSalida = jsonTransformer.toJson(respuesta);

			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			try {
				httpServletResponse.getWriter().println(jsonSalida);
			} catch (IOException ex1) {
				Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex1);
			}

		} catch (Exception ex) {
			httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Endpoint tipo PUT que actualiza la lista de propiedades de configuracion
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @param jsonSolicitud
	 */
	@RequestMapping(value = "/configuraciones", method = RequestMethod.PUT, consumes = Constantes.APPLICATION_JSON, produces = Constantes.APPLICATION_JSON)
	public void update(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			@RequestBody String jsonSolicitud) {
		Respuesta<Informacion> respuesta;
		String jsonSalida;
		RespuestaLogueo respuestaLogueo = null;
		Informacion informacion = null;
		try {
			SolicitudPropiedad solicitudPropiedad = (SolicitudPropiedad) jsonTransformer.fromJson(jsonSolicitud,
					SolicitudPropiedad.class);
			SolicitudLogueo solicitudBody = new SolicitudLogueo();
			solicitudBody.setContrasena(solicitudPropiedad.getContrasena());
			solicitudBody.setIdSistema(solicitudPropiedad.getIdSistema());
			solicitudBody.setLoginUsuario(solicitudPropiedad.getLoginUsuario());
			respuestaLogueo = usuarioConsume.autenticarMock(indicadorDAO, solicitudBody,
					solicitudPropiedad.getLoginUsuario(),
					indicadorDAO.obtenerPropiedad(Constantes.TOKEN_SERVICIO_USUARIO));
			if (respuestaLogueo != null) {
				informacion = new Informacion();
				for (Propiedad propiedad : solicitudPropiedad.getVariables()) {
					informacion.setResultado(indicadorDAO.updatePropiedad(propiedad.getId(), propiedad));
				}
				if (Constantes.CONF_ACTUALIZADO.equals(informacion.getResultado())) {
					informacion.setMensaje(Constantes.CONF_MENSAJE_EXITO_ACTUALIZAR);
				} else {
					informacion.setMensaje(Constantes.CONF_MENSAJE_ERROR_ACTUALIZAR);
				}
				respuesta = new Respuesta<Informacion>(CodigoEstatus.OK, informacion);
				jsonSalida = jsonTransformer.toJson(respuesta);
			} else {
				respuesta = new Respuesta<Informacion>(CodigoEstatus.OK, informacion);
				jsonSalida = jsonTransformer.toJson(respuesta);
			}

			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			httpServletResponse.setContentType("application/json; charset=UTF-8");
			httpServletResponse.getWriter().println(jsonSalida);

		} catch (IndicadorException ex) {
			respuesta = new Respuesta<Informacion>(ex);
			jsonSalida = jsonTransformer.toJson(respuesta);

			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			try {
				httpServletResponse.getWriter().println(jsonSalida);
			} catch (IOException ex1) {
				Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex1);
			}

		} catch (Exception ex) {
			httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Endpoint tipo POST que retorna la clave de una propiedad en especifica
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @param clave
	 * @param jsonSolicitud
	 */
	@RequestMapping(value = "/configuraciones/{clave}", method = RequestMethod.POST, consumes = Constantes.APPLICATION_JSON, produces = Constantes.APPLICATION_JSON)
	public void configuracionesClave(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			@PathVariable("clave") String clave, @RequestBody String jsonSolicitud) {
		Respuesta<Propiedad> respuesta;
		Propiedad propiedad = null;
		String jsonSalida = null;
		RespuestaLogueo respuestaLogueo = null;
		try {
			SolicitudLogueo solicitudBody = (SolicitudLogueo) jsonTransformer.fromJson(jsonSolicitud,
					SolicitudLogueo.class);
			solicitudBody.validar();
			respuestaLogueo = usuarioConsume.autenticarMock(indicadorDAO, solicitudBody,
					solicitudBody.getLoginUsuario(), indicadorDAO.obtenerPropiedad(Constantes.TOKEN_SERVICIO_USUARIO));
			if (respuestaLogueo != null) {
				String valor = indicadorDAO.obtenerPropiedad(StringUtils.upperCase(clave));
				if (valor != null && !valor.equals("FFFFFF")) {
					propiedad = new Propiedad();
					propiedad.setClave(clave);
					propiedad.setValor(valor);
					respuesta = new Respuesta<Propiedad>(CodigoEstatus.OK, propiedad);
					jsonSalida = jsonTransformer.toJson(respuesta);
				}else{
					respuesta = new Respuesta<Propiedad>(CodigoEstatus.OK, propiedad);
					jsonSalida = jsonTransformer.toJson(respuesta);
				}
			}else{
				respuesta = new Respuesta<Propiedad>(CodigoEstatus.OK, propiedad);
				jsonSalida = jsonTransformer.toJson(respuesta);
			}
			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			httpServletResponse.setContentType("application/json; charset=UTF-8");
			httpServletResponse.getWriter().println(jsonSalida);
		} catch (IndicadorException e) {
			respuesta = new Respuesta<Propiedad>(e);
			jsonSalida = jsonTransformer.toJson(respuesta);

			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			try {
				httpServletResponse.getWriter().println(jsonSalida);
			} catch (IOException ex1) {
				Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex1);
			}

		} catch (Exception ex) {
			httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

}
