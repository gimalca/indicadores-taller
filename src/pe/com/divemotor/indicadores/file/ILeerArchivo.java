package pe.com.divemotor.indicadores.file;

import java.io.IOException;
import java.util.List;

import pe.com.divemotor.indicadores.excepcion.IndicadorException;
import pe.com.divemotor.indicadores.persistencia.IIndicadorDAO;

/**
 * 
 * Declaracion de metodos disponibles para la lectura de csv
 *
 */
public interface ILeerArchivo<T> {

	List<T> leer(String archivo, IIndicadorDAO indicadorDAO, String codigoTalle, int idUsuarior) throws IndicadorException, IOException;
	
	List<T> leerFTP(String archivo, IIndicadorDAO indicadorDAO, String codigoTaller, int idUsuario) throws IndicadorException, IOException;
}
