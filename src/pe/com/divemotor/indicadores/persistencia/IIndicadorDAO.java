package pe.com.divemotor.indicadores.persistencia;

import java.util.List;

import pe.com.divemotor.indicadores.dominio.Menu;
import pe.com.divemotor.indicadores.dominio.Propiedad;
import pe.com.divemotor.indicadores.dominio.SolicitudUsuarioTaller;
import pe.com.divemotor.indicadores.dominio.Taller;
import pe.com.divemotor.indicadores.excepcion.IndicadorException;

/**
 * 
 * Metodos disponibles de acceso a los datos de la base de datos indicadores
 *
 */
public interface IIndicadorDAO {

	public String obtenerPropiedad(String clave) throws IndicadorException;

	public List<Menu> obtenerMenu(Integer idPerfil) throws IndicadorException;

	public List<Propiedad> listar() throws IndicadorException;

	public String updatePropiedad(int id, Propiedad propiedad) throws IndicadorException;

	public List<Taller> obtenerTalleresUsuario(int idUsuario) throws IndicadorException;

	public List<Taller> listarTaller() throws IndicadorException;

	public String updateUsuarioTaller(int id, SolicitudUsuarioTaller usuarioTaller) throws IndicadorException;
	
	public String agregarTaller(Taller taller) throws IndicadorException;

}
