package pe.com.divemotor.indicadores.persistencia.implementacion;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import oracle.jdbc.OracleTypes;
import pe.com.divemotor.indicadores.dominio.UsuarioTaller;
import pe.com.divemotor.indicadores.dominio.mapper.UsuarioTallerMapper;
import pe.com.divemotor.indicadores.excepcion.IndicadorException;
import pe.com.divemotor.indicadores.persistencia.IUsuarioDAO;
import pe.com.divemotor.indicadores.util.CodigoEstatus;

/**
 * 
 * Clase de implementacion de DB de usuarios para realizar la busqueda
 *
 */
public class UsuarioDAOImpl implements IUsuarioDAO {

	private JdbcTemplate jdbcTemplate;
	private DataSource dataSourceLogin;
	private ApplicationContext context;

	/**
	 * Stored procedure	 que realiza la busqueda no se esta llamando por que falla
	 * 
	 **/
	@Override
	public List<UsuarioTaller> obtenerUsuarios(String busqueda, int idSitema) throws IndicadorException {
		Logger.getLogger(UsuarioDAOImpl.class.getName()).log(Level.INFO, "{INICIO} obtenerUsuarios");
		List<UsuarioTaller> listaUsuarioTaller = null;
		String sql = "{CALL CONSULTAR_USUARIOS(?,?,?)}";
		CallableStatement callableSt = null;
		ResultSet rs = null;
		Connection conn = null;
		Logger.getLogger(UsuarioDAOImpl.class.getName()).log(Level.INFO, "{CUERPO} antes del try");
		try {
			jdbcTemplate = new JdbcTemplate(dataSourceLogin);
			conn = jdbcTemplate.getDataSource().getConnection();
			callableSt = conn.prepareCall(sql);
			Logger.getLogger(UsuarioDAOImpl.class.getName()).log(Level.INFO, "{CUERPO} despues del preparecallable");
			callableSt.setInt(1, idSitema);
			callableSt.setString(2, busqueda);
			callableSt.registerOutParameter(3, OracleTypes.CURSOR);

			callableSt.executeUpdate();
			Logger.getLogger(UsuarioDAOImpl.class.getName()).log(Level.INFO, "{CUERPO} despues de ejecutar callable");
			rs = (ResultSet) callableSt.getObject(3);
			listaUsuarioTaller = new ArrayList<UsuarioTaller>();
			Logger.getLogger(UsuarioDAOImpl.class.getName()).log(Level.INFO, "{CUERPO} despues del rs");
			while (rs.next()) {				
				UsuarioTaller usuarioTaller = new UsuarioTaller();
				usuarioTaller.setIdUsuario(rs.getInt("ID_USUARIO"));
				usuarioTaller.setNombre(rs.getString("NOMBRE"));
				usuarioTaller.setApellido(rs.getString("APELLIDO"));
				usuarioTaller.setCorreo(rs.getString("CORREO"));
				usuarioTaller.setLogin(rs.getString("LOGIN"));
				listaUsuarioTaller.add(usuarioTaller);
				Logger.getLogger(UsuarioDAOImpl.class.getName()).log(Level.INFO, "{CUERPO} obtenerUsuarios " + usuarioTaller.getNombre());
			}
		} catch (SQLException e) {
			Logger.getLogger(UsuarioDAOImpl.class.getName()).log(Level.INFO, null, "obtenerUsuarios" + e.getMessage());
			IndicadorException ie = IndicadorException
					.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_PROVEEDOR_PERISTENCIA, e.getMessage(), e);
			ie.getTraza().getErrors().add(e.getMessage());
			throw ie;
		} finally {
			try {
				if (!callableSt.isClosed()) {
					callableSt.close();
				}
				if (!rs.isClosed()) {
					rs.close();
				}
				if (!conn.isClosed()) {
					conn.close();
				}				
			} catch (SQLException e) {
				Logger.getLogger(UsuarioDAOImpl.class.getName()).log(Level.INFO, null, "obtenerUsuarios" + e.getMessage());
				IndicadorException ie = IndicadorException
						.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_PROVEEDOR_PERISTENCIA, e.getMessage(), e);
				ie.getTraza().getErrors().add(e.getMessage());
				throw ie;
			}
		}
		Logger.getLogger(UsuarioDAOImpl.class.getName()).log(Level.INFO, "{FIN} obtenerUsuarios");
		return listaUsuarioTaller;
	}
	
	/**
	 * Scripts que realiza la busqueda es el que esta activo!
	 */
	@Override
	public List<UsuarioTaller> obtenerUsuariosScript(String busqueda, int idSistema) throws IndicadorException {
		Logger.getLogger(UsuarioDAOImpl.class.getName()).log(Level.INFO, "{INICIO} obtenerUsuarios");
		jdbcTemplate = new JdbcTemplate(dataSourceLogin);
		List<UsuarioTaller> listaUsuarioTaller = null;
		String sql = "SELECT U.COD_ID_USUARIO AS ID_USUARIO, U.TXT_NOMBRES AS NOMBRE, U.TXT_APELLIDOS AS APELLIDO, U.TXT_CORREO AS CORREO, U.TXT_USUARIO AS LOGIN "
				+ "FROM SISTEMAS.SIS_MAE_USUARIO U "
				+ "INNER JOIN SISTEMAS.SIS_MAE_PERFIL_USUARIO SMPU on U.COD_ID_USUARIO=SMPU.COD_ID_USUARIO "
				+ "INNER JOIN SISTEMAS.SIS_MAE_PERFIL SMP on SMPU.COD_ID_PERFIL=SMP.COD_ID_PERFIL "
				+ "INNER JOIN SISTEMAS.SIS_MAE_SISTEMA SMS on SMP.COD_ID_SISTEMA =SMS.COD_ID_SISTEMA "
				+ "WHERE SMP.COD_ID_SISTEMA = ? AND SMPU.IND_INACTIVO = 'N' "
				+ "AND (TRANSLATE(UPPER(U.TXT_NOMBRES), 'ÁÉÍÓÚÀÈÌÒÙÄËÏÖÜÂÊÎÔÛÑ', 'AEIOUAEIOUAEIOUAEIOUN') LIKE UPPER(?) "
				+ "OR TRANSLATE(UPPER(U.TXT_APELLIDOS), 'ÁÉÍÓÚÀÈÌÒÙÄËÏÖÜÂÊÎÔÛÑ', 'AEIOUAEIOUAEIOUAEIOUN') LIKE UPPER(?)) "
				+ "ORDER BY U.TXT_APELLIDOS ASC";
		listaUsuarioTaller = jdbcTemplate.query(sql, new Object[] {idSistema, busqueda, busqueda}, new UsuarioTallerMapper());
		Logger.getLogger(UsuarioDAOImpl.class.getName()).log(Level.INFO, "{FIN} obtenerUsuarios");
		return listaUsuarioTaller;
	}

	public DataSource getDataSourceLogin() {
		return dataSourceLogin;
	}

	public void setDataSourceLogin(DataSource dataSourceLogin) {
		this.dataSourceLogin = dataSourceLogin;
	}

	public UsuarioDAOImpl getUsuarioDAOImpl() {
		context = new ClassPathXmlApplicationContext("jdbc-config.xml");
		return (UsuarioDAOImpl) context.getBean("usuarioDAO");
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
