package pe.com.divemotor.indicadores.dominio;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * Clase de dominio de campos en el csv
 *
 */
public class Tecnico implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codigoCentro;
	private String codigoTaller;
	private int mes;
	private int anio;
	private String codigoMecanico;
	private String descripcionCentro;
	private String descripcionTaller;
	private String nombreMecanico;
	private String hrsPres;
	private String hrsReal;
	private String hrsNot;
	private BigDecimal porcentajeProductividad;
	private String codigoColorProductividad;
	private BigDecimal porcentajeEficiencia;
	private String codigoColorEficiencia;
	private BigDecimal porcentajeEfectividad;
	private String codigoColorEfectividad;
	private String imgLink;
	private int totalPaginas;

	public String getCodigoCentro() {
		return codigoCentro;
	}

	public void setCodigoCentro(String codigoCentro) {
		this.codigoCentro = codigoCentro;
	}

	public String getCodigoTaller() {
		return codigoTaller;
	}

	public void setCodigoTaller(String codigoTaller) {
		this.codigoTaller = codigoTaller;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public String getDescripcionCentro() {
		return descripcionCentro;
	}

	public void setDescripcionCentro(String descripcionCentro) {
		this.descripcionCentro = descripcionCentro;
	}

	public String getDescripcionTaller() {
		return descripcionTaller;
	}

	public void setDescripcionTaller(String descripcionTaller) {
		this.descripcionTaller = descripcionTaller;
	}

	public BigDecimal getPorcentajeProductividad() {
		return porcentajeProductividad;
	}

	public void setPorcentajeProductividad(BigDecimal porcentajeProductividad) {
		this.porcentajeProductividad = porcentajeProductividad;
	}

	public String getCodigoColorProductividad() {
		return codigoColorProductividad;
	}

	public void setCodigoColorProductividad(String codigoColorProductividad) {
		this.codigoColorProductividad = codigoColorProductividad;
	}

	public String getCodigoColorEficiencia() {
		return codigoColorEficiencia;
	}

	public void setCodigoColorEficiencia(String codigoColorEficiencia) {
		this.codigoColorEficiencia = codigoColorEficiencia;
	}

	public BigDecimal getPorcentajeEficiencia() {
		return porcentajeEficiencia;
	}

	public void setPorcentajeEficiencia(BigDecimal porcentajeEficiencia) {
		this.porcentajeEficiencia = porcentajeEficiencia;
	}

	public BigDecimal getPorcentajeEfectividad() {
		return porcentajeEfectividad;
	}

	public void setPorcentajeEfectividad(BigDecimal porcentajeEfectividad) {
		this.porcentajeEfectividad = porcentajeEfectividad;
	}

	public String getCodigoColorEfectividad() {
		return codigoColorEfectividad;
	}

	public void setCodigoColorEfectividad(String codigoColorEfectividad) {
		this.codigoColorEfectividad = codigoColorEfectividad;
	}

	public String getCodigoMecanico() {
		return codigoMecanico;
	}

	public void setCodigoMecanico(String codigoMecanico) {
		this.codigoMecanico = codigoMecanico;
	}

	public String getNombreMecanico() {
		return nombreMecanico;
	}

	public void setNombreMecanico(String nombreMecanico) {
		this.nombreMecanico = nombreMecanico;
	}

	public String getHrsPres() {
		return hrsPres;
	}

	public void setHrsPres(String hrsPres) {
		this.hrsPres = hrsPres;
	}

	public String getHrsReal() {
		return hrsReal;
	}

	public void setHrsReal(String hrsReal) {
		this.hrsReal = hrsReal;
	}

	public String getHrsNot() {
		return hrsNot;
	}

	public void setHrsNot(String hrsNot) {
		this.hrsNot = hrsNot;
	}

	public String getImgLink() {
		return imgLink;
	}

	public void setImgLink(String imgLink) {
		this.imgLink = imgLink;
	}

	public int getTotalPaginas() {
		return totalPaginas;
	}

	public void setTotalPaginas(int totalPaginas) {
		this.totalPaginas = totalPaginas;
	}

}
