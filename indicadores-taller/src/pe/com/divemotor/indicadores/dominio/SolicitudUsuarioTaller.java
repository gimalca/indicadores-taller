package pe.com.divemotor.indicadores.dominio;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * Clase de dominio de ids de talleres para asignacion
 *
 */
public class SolicitudUsuarioTaller implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<String> idsTalleres;

	public List<String> getIdsTalleres() {
		return idsTalleres;
	}

	public void setIdsTalleres(List<String> idsTalleres) {
		this.idsTalleres = idsTalleres;
	}

}
