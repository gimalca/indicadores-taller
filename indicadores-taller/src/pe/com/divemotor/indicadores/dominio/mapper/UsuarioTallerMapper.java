package pe.com.divemotor.indicadores.dominio.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.divemotor.indicadores.dominio.UsuarioTaller;

/**
 * 
 * Clase de mapeo de UsuarioTaller para el retorno de la busqueda 
 *
 */
public class UsuarioTallerMapper implements RowMapper<UsuarioTaller> {

	@Override
	public UsuarioTaller mapRow(ResultSet rs, int rowNum) throws SQLException {
		UsuarioTaller usuarioTaller = new UsuarioTaller();
		usuarioTaller.setIdUsuario(rs.getInt("ID_USUARIO"));
		usuarioTaller.setNombre(rs.getString("NOMBRE"));
		usuarioTaller.setApellido(rs.getString("APELLIDO"));
		usuarioTaller.setCorreo(rs.getString("CORREO"));
		usuarioTaller.setLogin(rs.getString("LOGIN"));
		return usuarioTaller;
	}
}
