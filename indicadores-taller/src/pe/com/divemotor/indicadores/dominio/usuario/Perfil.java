package pe.com.divemotor.indicadores.dominio.usuario;

import java.io.Serializable;
import java.util.List;

import pe.com.divemotor.indicadores.dominio.Taller;

public class Perfil implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nombre;
	private String cargo;
	private String telefono;
	private String centro;
	private List<Taller> talleres;
	private String imgLink;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCentro() {
		return centro;
	}

	public void setCentro(String centro) {
		this.centro = centro;
	}

	public List<Taller> getTalleres() {
		return talleres;
	}

	public void setTalleres(List<Taller> talleres) {
		this.talleres = talleres;
	}

	public String getImgLink() {
		return imgLink;
	}

	public void setImgLink(String imgLink) {
		this.imgLink = imgLink;
	}

}
