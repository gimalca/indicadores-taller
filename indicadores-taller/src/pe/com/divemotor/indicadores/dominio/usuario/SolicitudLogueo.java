package pe.com.divemotor.indicadores.dominio.usuario;

import java.io.Serializable;
import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;

import pe.com.divemotor.indicadores.excepcion.IndicadorException;
import pe.com.divemotor.indicadores.util.CodigoEstatus;

public class SolicitudLogueo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String contrasena;
	private int idSistema;
	private String loginUsuario;

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public String getLoginUsuario() {
		return loginUsuario;
	}

	public void setLoginUsuario(String loginUsuario) {
		this.loginUsuario = loginUsuario;
	}

	public int getIdSistema() {
		return idSistema;
	}

	public void setIdSistema(int idSistema) {
		this.idSistema = idSistema;
	}

	public void validar() throws IndicadorException {
		IndicadorException excepcion = IndicadorException.getInstanciaDesdeMensaje(CodigoEstatus.ERROR_EN_SOLICITUD,
				IndicadorException.ERROR_PROPIEDAD_NULA_VACIA);

		if (StringUtils.isBlank(getContrasena())) {
			excepcion.getTraza().getErrors()
					.add(MessageFormat.format(IndicadorException.ERROR_PROPIEDAD_NULA_VACIA, getContrasena()));
			throw excepcion;
		}
	}
}
