package pe.com.divemotor.indicadores.dominio.usuario;

import java.io.Serializable;

public class Negocio implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String noCia;
	private String nombreCia;
	private String codAreaVta;
	private String desAreaVta;
	private String codFilial;
	private String nombreFilial;
	private int codFamilia;
	private String desFamilia;
	private String codMarca;
	private String nombreMarca;
	private String indComercial;
	private boolean indComercialBoolean;
	private String usuarioSid;

	public String getNoCia() {
		return noCia;
	}

	public void setNoCia(String noCia) {
		this.noCia = noCia;
	}

	public String getNombreCia() {
		return nombreCia;
	}

	public void setNombreCia(String nombreCia) {
		this.nombreCia = nombreCia;
	}

	public String getCodAreaVta() {
		return codAreaVta;
	}

	public void setCodAreaVta(String codAreaVta) {
		this.codAreaVta = codAreaVta;
	}

	public String getDesAreaVta() {
		return desAreaVta;
	}

	public void setDesAreaVta(String desAreaVta) {
		this.desAreaVta = desAreaVta;
	}

	public String getCodFilial() {
		return codFilial;
	}

	public void setCodFilial(String codFilial) {
		this.codFilial = codFilial;
	}

	public String getNombreFilial() {
		return nombreFilial;
	}

	public void setNombreFilial(String nombreFilial) {
		this.nombreFilial = nombreFilial;
	}

	public int getCodFamilia() {
		return codFamilia;
	}

	public void setCodFamilia(int codFamilia) {
		this.codFamilia = codFamilia;
	}

	public String getDesFamilia() {
		return desFamilia;
	}

	public void setDesFamilia(String desFamilia) {
		this.desFamilia = desFamilia;
	}

	public String getCodMarca() {
		return codMarca;
	}

	public void setCodMarca(String codMarca) {
		this.codMarca = codMarca;
	}

	public String getNombreMarca() {
		return nombreMarca;
	}

	public void setNombreMarca(String nombreMarca) {
		this.nombreMarca = nombreMarca;
	}

	public String getIndComercial() {
		return indComercial;
	}

	public void setIndComercial(String indComercial) {
		this.indComercial = indComercial;
	}

	public boolean isIndComercialBoolean() {
		return indComercialBoolean;
	}

	public void setIndComercialBoolean(boolean indComercialBoolean) {
		this.indComercialBoolean = indComercialBoolean;
	}

	public String getUsuarioSid() {
		return usuarioSid;
	}

	public void setUsuarioSid(String usuarioSid) {
		this.usuarioSid = usuarioSid;
	}

}
