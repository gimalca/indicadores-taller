package pe.com.divemotor.indicadores.presentacion;

import java.io.Serializable;

/**
 * 
 * Clase de presentacion de mensajes
 *
 */
public class Informacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String resultado;
	private String mensaje;

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
