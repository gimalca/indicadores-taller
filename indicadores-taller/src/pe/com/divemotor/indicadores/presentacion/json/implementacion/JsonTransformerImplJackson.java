package pe.com.divemotor.indicadores.presentacion.json.implementacion;

import java.io.BufferedReader;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.divemotor.indicadores.presentacion.RespuestaLogueo;
import pe.com.divemotor.indicadores.presentacion.json.IJsonTransformer;
/**
 * 
 * Implementacion de metodos para manejar los JSON
 *
 */
public class JsonTransformerImplJackson implements IJsonTransformer {

	@Override
	public String toJson(Object data) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();

			return objectMapper.writeValueAsString(data);
		} catch (JsonProcessingException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public Object fromJson(String json, Class clazz) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();

			return objectMapper.readValue(json, clazz);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public Object extraer(BufferedReader br) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			return objectMapper.readValue(br, new TypeReference<RespuestaLogueo>() {});
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

}
