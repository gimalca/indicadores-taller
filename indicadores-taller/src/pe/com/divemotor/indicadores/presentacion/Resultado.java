package pe.com.divemotor.indicadores.presentacion;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * Clase que envuelve la respuesta de los servicios
 *
 */
public class Resultado<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean valido;
	private List<T> listado;

	public boolean isValido() {
		return valido;
	}

	public void setValido(boolean valido) {
		this.valido = valido;
	}

	public List<T> getListado() {
		return listado;
	}

	public void setListado(List<T> listado) {
		this.listado = listado;
	}

}
