package pe.com.divemotor.indicadores.presentacion;

import java.io.Serializable;
import java.util.List;

import pe.com.divemotor.indicadores.dominio.usuario.Configuracion;
import pe.com.divemotor.indicadores.dominio.usuario.Negocio;
import pe.com.divemotor.indicadores.dominio.usuario.Opcion;
import pe.com.divemotor.indicadores.dominio.usuario.Proceso;
import pe.com.divemotor.indicadores.dominio.usuario.Usuario;

/**
 * 
 * Clase de respuesta del servicio externo de Login
 *
 */
public class RespuestaLogueo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int estadoTrama;
	private Usuario usuario;
	private List<Opcion> opcionList = null;
	private List<Integer> perfilList = null;
	private List<Proceso> procesoList = null;
	private List<Configuracion> configuracionList = null;
	private Negocio negocio;
	private boolean ingreso;
	private String mensaje;

	public int getEstadoTrama() {
		return estadoTrama;
	}

	public void setEstadoTrama(int estadoTrama) {
		this.estadoTrama = estadoTrama;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Opcion> getOpcionList() {
		return opcionList;
	}

	public void setOpcionList(List<Opcion> opcionList) {
		this.opcionList = opcionList;
	}

	public List<Integer> getPerfilList() {
		return perfilList;
	}

	public void setPerfilList(List<Integer> perfilList) {
		this.perfilList = perfilList;
	}

	public List<Proceso> getProcesoList() {
		return procesoList;
	}

	public void setProcesoList(List<Proceso> procesoList) {
		this.procesoList = procesoList;
	}

	public List<Configuracion> getConfiguracionList() {
		return configuracionList;
	}

	public void setConfiguracionList(List<Configuracion> configuracionList) {
		this.configuracionList = configuracionList;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public boolean isIngreso() {
		return ingreso;
	}

	public void setIngreso(boolean ingreso) {
		this.ingreso = ingreso;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
