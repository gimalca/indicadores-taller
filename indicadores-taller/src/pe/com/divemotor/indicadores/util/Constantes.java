package pe.com.divemotor.indicadores.util;

/**
 * 
 * Constantes generales para la aplicacion
 *
 */
public class Constantes {

	public final static String CONF_URL_ARCHIVOS = "CONF_URL_ARCHIVOS";
	public final static String CONF_NOMBRE_ARCHIVO_FACTURACION = "CONF_NOMBRE_ARCHIVO_FACTURACION";
	public final static String CONF_NOMBRE_ARCHIVO_TECNICOS = "CONF_NOMBRE_ARCHIVO_TECNICOS";
	public final static String CONF_NOMBRE_ARCHIVO_ENTREGAS = "CONF_NOMBRE_ARCHIVO_ENTREGAS";
	public final static String CONF_COLOR = "CONF_COLOR_";
	public final static String CONF_MONEDA = "CONF_MONEDA_";
	public final static String CONF_FTP_USUARIO = "CONF_FTP_USUARIO";
	public final static String CONF_FTP_PASSWORD = "CONF_FTP_PASSWORD";
	public final static String CONF_FTP_SERVIDOR = "CONF_FTP_SERVIDOR";
	public final static String CONF_FTP_ACTIVADO = "CONF_FTP_ACTIVADO";
	public final static String CONF_FTP_ACTIVO = "S";
	public static final String EXTENSION = ".csv";
	public static final String CONF_URL_IMAGE_MECANICOS = "CONF_URL_IMAGE_MECANICOS";
	public static final String CONF_EXTENSION_IMAGE_MECANICOS = ".jpg";
	public static final String CONF_TOKEN_IMAGE_MECANICOS = "CONF_TOKEN_IMAGE_MECANICOS";
	public static final String CONF_PRETOKEN_IMAGE_MECANICOS = "?alt=media&token=";
	
	public static final String CONF_URL_SERVICIO_AUTENTICAR = "CONF_URL_SERVICIO_AUTENTICAR";
	public static final String TOKEN_SERVICIO_USUARIO = "TOKEN_SERVICIO_USUARIO";
	
	public static final String APPLICATION_JSON = "application/json";
	public static final String CONTENT_TYPE = "application/json; charset=UTF-8";
	public static final String PORCENTAJE = "%";
	public static final String CONF_ID_SISTEMA = "CONF_ID_SISTEMA";
	public static final String CONF_MENSAJE_ERROR_USUARIO = "El usuario y/o la contraseña son incorrecto";
	public static final String CONF_ACTUALIZADO = "1000";
	public static final String CONF_MENSAJE_ERROR_ACTUALIZAR = "No se pudo Actualizar";
	public static final String CONF_MENSAJE_EXITO_ACTUALIZAR = "Registro Actualizado";
	public static final int CONF_TROZAR_TECNICOS = 3;
	public static final int CONF_TROZAR_ENTREGAS = 5;

}
