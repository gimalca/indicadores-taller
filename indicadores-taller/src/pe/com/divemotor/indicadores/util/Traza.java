package pe.com.divemotor.indicadores.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Lista de errores
 *
 */
public class Traza implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final List<String> errors;

	private Traza() {
		this.errors = new ArrayList<String>();
	}

	public List<String> getErrors() {
		return errors;
	}

	public final static Traza getInstancia() {
		return new Traza();
	}

}
