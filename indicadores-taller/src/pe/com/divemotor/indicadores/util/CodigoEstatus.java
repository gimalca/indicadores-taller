package pe.com.divemotor.indicadores.util;

/**
 * 
 * Enum de errores
 *
 */
public enum CodigoEstatus {
	OK(200), ERROR_EN_SOLICITUD(400), ERROR_EN_PARAMETRO(422), RECURSO_NO_ENCONTRADO(404), ERROR_INTERNO_SERVIDOR(
			500), ERROR_LECTURA_INTERNA_SERVIDOR(501), ERROR_PROVEEDOR_PERISTENCIA(561);

	private int valor;

	private CodigoEstatus(int codigo) {
		this.setCodigo(codigo);
	}

	public int getCodigo() {
		return valor;
	}

	private void setCodigo(int valor) {
		this.valor = valor;
	}
}
