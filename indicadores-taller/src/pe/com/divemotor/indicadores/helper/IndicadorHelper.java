package pe.com.divemotor.indicadores.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pe.com.divemotor.indicadores.dominio.Entrega;
import pe.com.divemotor.indicadores.dominio.Tecnico;
import pe.com.divemotor.indicadores.excepcion.IndicadorException;
import pe.com.divemotor.indicadores.file.ILeerArchivo;
import pe.com.divemotor.indicadores.file.implementacion.LeerArchivoImplEntrega;
import pe.com.divemotor.indicadores.file.implementacion.LeerArchivoImplTecnico;
import pe.com.divemotor.indicadores.persistencia.IIndicadorDAO;
import pe.com.divemotor.indicadores.presentacion.Respuesta;
import pe.com.divemotor.indicadores.presentacion.Resultado;
import pe.com.divemotor.indicadores.presentacion.json.IJsonTransformer;
import pe.com.divemotor.indicadores.util.CodigoEstatus;
import pe.com.divemotor.indicadores.util.Constantes;

/**
 * 
 * Clase que permite dividir las lista de tecnicos y entregas
 *
 */
public class IndicadorHelper {

	public List<List<Tecnico>> obtenerTecnicos(IIndicadorDAO indicadorDAO, ILeerArchivo<Tecnico> archivo,
			String codigoTaller) {
		Resultado<Tecnico> resultado = new Resultado<Tecnico>();
		List<List<Tecnico>> listaTecnico = null;
		try {
			resultado.setListado(archivo.leer(
					indicadorDAO.obtenerPropiedad(Constantes.CONF_URL_ARCHIVOS)
							+ indicadorDAO.obtenerPropiedad(Constantes.CONF_NOMBRE_ARCHIVO_TECNICOS),
					indicadorDAO, codigoTaller, 0));
			resultado.setValido(true);
			int trozar_por = Constantes.CONF_TROZAR_TECNICOS;
			int iPasoCotaInferior = 0;
			int iPasoCotaSuperior = resultado.getListado().size() < trozar_por ? resultado.getListado().size()
					: trozar_por;
			listaTecnico = new ArrayList<List<Tecnico>>();
			while (iPasoCotaInferior <= resultado.getListado().size()) {
				listaTecnico.add(trozarLista(resultado.getListado(), iPasoCotaInferior, iPasoCotaSuperior));
				iPasoCotaInferior = iPasoCotaInferior + trozar_por;
				iPasoCotaSuperior = iPasoCotaSuperior + trozar_por > resultado.getListado().size()
						? resultado.getListado().size() : iPasoCotaSuperior + trozar_por;
			}
			for (List<Tecnico> list : listaTecnico) {
				for (Tecnico tecnico : list) {
					if (resultado.getListado().size() % trozar_por == 0) {
						tecnico.setTotalPaginas(resultado.getListado().size() / trozar_por);
					} else {
						tecnico.setTotalPaginas((resultado.getListado().size() / trozar_por) + 1);
					}
				}
			}
		} catch (IndicadorException | IOException e) {
			listaTecnico = null;
		}
		return listaTecnico;
	}

	public String obtenerResultado(List<Tecnico> tecnicos, IJsonTransformer jsonTransformer) {
		String jsonSalida = "";
		Respuesta<Resultado<Tecnico>> respuesta = null;
		Resultado<Tecnico> resultado = new Resultado<Tecnico>();

		resultado.setListado(tecnicos);
		resultado.setValido(true);

		respuesta = new Respuesta<Resultado<Tecnico>>(CodigoEstatus.OK, resultado);
		jsonSalida = jsonTransformer.toJson(respuesta);
		return jsonSalida;
	}

	private static <T> List<T> trozarLista(List<T> list, int iCotaInferior, int iCotaSuperior) {
		iCotaSuperior = iCotaSuperior > list.size() ? list.size() - 1 : iCotaSuperior;
		List<T> x = new ArrayList<T>(list.subList(iCotaInferior, iCotaSuperior));
		return x;
	}

	public List<List<Tecnico>> obtenerTecnicosFTP(IIndicadorDAO indicadorDAO, LeerArchivoImplTecnico archivoTecnico,
			String codigoTaller) {
		Resultado<Tecnico> resultado = new Resultado<Tecnico>();
		List<List<Tecnico>> listaTecnico = null;
		try {
			resultado.setListado(archivoTecnico.leerFTP(
					indicadorDAO.obtenerPropiedad(Constantes.CONF_URL_ARCHIVOS)
							+ indicadorDAO.obtenerPropiedad(Constantes.CONF_NOMBRE_ARCHIVO_TECNICOS),
					indicadorDAO, codigoTaller, 0));
			resultado.setValido(true);
			int trozar_por = Constantes.CONF_TROZAR_TECNICOS;
			int iPasoCotaInferior = 0;
			int iPasoCotaSuperior = resultado.getListado().size() < trozar_por ? resultado.getListado().size()
					: trozar_por;
			listaTecnico = new ArrayList<List<Tecnico>>();
			while (iPasoCotaInferior <= resultado.getListado().size()) {
				listaTecnico.add(trozarLista(resultado.getListado(), iPasoCotaInferior, iPasoCotaSuperior));
				iPasoCotaInferior = iPasoCotaInferior + trozar_por;
				iPasoCotaSuperior = iPasoCotaSuperior + trozar_por > resultado.getListado().size()
						? resultado.getListado().size() : iPasoCotaSuperior + trozar_por;
			}
			for (List<Tecnico> list : listaTecnico) {
				for (Tecnico tecnico : list) {
					if (resultado.getListado().size() % trozar_por == 0) {
						tecnico.setTotalPaginas(resultado.getListado().size() / trozar_por);
					} else {
						tecnico.setTotalPaginas((resultado.getListado().size() / trozar_por) + 1);
					}
				}
			}
		} catch (IndicadorException | IOException e) {
			listaTecnico = null;
		}
		return listaTecnico;
	}

	public String obtenerResultadoFTP(List<Tecnico> tecnicos, IJsonTransformer jsonTransformer) {
		String jsonSalida = "";
		Respuesta<Resultado<Tecnico>> respuesta = null;
		Resultado<Tecnico> resultado = new Resultado<Tecnico>();

		resultado.setListado(tecnicos);
		resultado.setValido(true);

		respuesta = new Respuesta<Resultado<Tecnico>>(CodigoEstatus.OK, resultado);
		jsonSalida = jsonTransformer.toJson(respuesta);
		return jsonSalida;
	}

	public List<List<Entrega>> obtenerEntregasFTP(IIndicadorDAO indicadorDAO, LeerArchivoImplEntrega archivo,
			String codigoTaller) {
		Resultado<Entrega> resultado = new Resultado<Entrega>();
		List<List<Entrega>> listaEntrega = null;
		try {
			resultado.setListado(archivo.leerFTP(
					indicadorDAO.obtenerPropiedad(Constantes.CONF_URL_ARCHIVOS)
							+ indicadorDAO.obtenerPropiedad(Constantes.CONF_NOMBRE_ARCHIVO_ENTREGAS),
					indicadorDAO, codigoTaller, 0));
			resultado.setValido(true);
			int trozar_por = Constantes.CONF_TROZAR_ENTREGAS;
			int iPasoCotaInferior = 0;
			int iPasoCotaSuperior = resultado.getListado().size() < trozar_por ? resultado.getListado().size()
					: trozar_por;
			listaEntrega = new ArrayList<List<Entrega>>();
			while (iPasoCotaInferior <= resultado.getListado().size()) {
				listaEntrega.add(trozarLista(resultado.getListado(), iPasoCotaInferior, iPasoCotaSuperior));
				iPasoCotaInferior = iPasoCotaInferior + trozar_por;
				iPasoCotaSuperior = iPasoCotaSuperior + trozar_por > resultado.getListado().size()
						? resultado.getListado().size() : iPasoCotaSuperior + trozar_por;
			}
			for (List<Entrega> list : listaEntrega) {
				for (Entrega entrega : list) {
					if (resultado.getListado().size() % trozar_por == 0) {
						entrega.setTotalPaginas(resultado.getListado().size() / trozar_por);
					} else {
						entrega.setTotalPaginas((resultado.getListado().size() / trozar_por) + 1);
					}
				}
			}
		} catch (IndicadorException | IOException e) {
			listaEntrega = null;
		}
		return listaEntrega;
	}

	public List<List<Entrega>> obtenerEntregas(IIndicadorDAO indicadorDAO, LeerArchivoImplEntrega archivo,
			String codigoTaller) {
		Resultado<Entrega> resultado = new Resultado<Entrega>();
		List<List<Entrega>> listaEntrega = null;
		try {
			resultado.setListado(archivo.leer(
					indicadorDAO.obtenerPropiedad(Constantes.CONF_URL_ARCHIVOS)
							+ indicadorDAO.obtenerPropiedad(Constantes.CONF_NOMBRE_ARCHIVO_ENTREGAS),
					indicadorDAO, codigoTaller, 0));
			resultado.setValido(true);
			int trozar_por = Constantes.CONF_TROZAR_ENTREGAS;
			int iPasoCotaInferior = 0;
			int iPasoCotaSuperior = resultado.getListado().size() < trozar_por ? resultado.getListado().size()
					: trozar_por;
			listaEntrega = new ArrayList<List<Entrega>>();
			while (iPasoCotaInferior <= resultado.getListado().size()) {
				listaEntrega.add(trozarLista(resultado.getListado(), iPasoCotaInferior, iPasoCotaSuperior));
				iPasoCotaInferior = iPasoCotaInferior + trozar_por;
				iPasoCotaSuperior = iPasoCotaSuperior + trozar_por > resultado.getListado().size()
						? resultado.getListado().size() : iPasoCotaSuperior + trozar_por;
			}
			for (List<Entrega> list : listaEntrega) {
				for (Entrega entrega : list) {
					if (resultado.getListado().size() % trozar_por == 0) {
						entrega.setTotalPaginas(resultado.getListado().size() / trozar_por);
					} else {
						entrega.setTotalPaginas((resultado.getListado().size() / trozar_por) + 1);
					}
				}
			}
		} catch (IndicadorException | IOException e) {
			listaEntrega = null;
		}
		return listaEntrega;
	}

	public String obtenerResultadoEntregaFTP(List<Entrega> entregas, IJsonTransformer jsonTransformer) {
		String jsonSalida = "";
		Respuesta<Resultado<Entrega>> respuesta = null;
		Resultado<Entrega> resultado = new Resultado<Entrega>();

		resultado.setListado(entregas);
		resultado.setValido(true);

		respuesta = new Respuesta<Resultado<Entrega>>(CodigoEstatus.OK, resultado);
		jsonSalida = jsonTransformer.toJson(respuesta);
		return jsonSalida;
	}

}