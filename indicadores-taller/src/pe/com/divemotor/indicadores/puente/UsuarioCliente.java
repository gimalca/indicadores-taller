package pe.com.divemotor.indicadores.puente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import pe.com.divemotor.indicadores.dominio.Taller;
import pe.com.divemotor.indicadores.dominio.UsuarioTaller;
import pe.com.divemotor.indicadores.dominio.usuario.Configuracion;
import pe.com.divemotor.indicadores.dominio.usuario.HijoOpcion;
import pe.com.divemotor.indicadores.dominio.usuario.Negocio;
import pe.com.divemotor.indicadores.dominio.usuario.Opcion;
import pe.com.divemotor.indicadores.dominio.usuario.Perfil;
import pe.com.divemotor.indicadores.dominio.usuario.Proceso;
import pe.com.divemotor.indicadores.dominio.usuario.SolicitudLogueo;
import pe.com.divemotor.indicadores.dominio.usuario.Usuario;
import pe.com.divemotor.indicadores.excepcion.IndicadorException;
import pe.com.divemotor.indicadores.persistencia.IIndicadorDAO;
import pe.com.divemotor.indicadores.persistencia.IUsuarioDAO;
import pe.com.divemotor.indicadores.presentacion.RespuestaLogueo;
import pe.com.divemotor.indicadores.presentacion.json.IJsonTransformer;
import pe.com.divemotor.indicadores.presentacion.json.implementacion.JsonTransformerImplJackson;
import pe.com.divemotor.indicadores.util.CodigoEstatus;
import pe.com.divemotor.indicadores.util.Constantes;

/**
 * Permite realizar el puente con el servicio de Login
 * @author 
 *
 */
public class UsuarioCliente {

	/**
	 * Metodo que invoca el servicio Login para validaciones, este se usara como seguridad
	 * para la aplicacion completa
	 * @param indicadorDAO
	 * @param solicitudBody
	 * @param coddUsuario
	 * @param token
	 * @return
	 * @throws IndicadorException
	 */
	public RespuestaLogueo autenticar(IIndicadorDAO indicadorDAO, SolicitudLogueo solicitudBody, String coddUsuario, String token)
			throws IndicadorException {
		RespuestaLogueo respuestaLogueo = null;
		BufferedReader br = null;
		try {
			IJsonTransformer transformed = new JsonTransformerImplJackson();
			solicitudBody.setIdSistema(Integer.parseInt(indicadorDAO.obtenerPropiedad(Constantes.CONF_ID_SISTEMA)));
			String solicitudJson = transformed.toJson(solicitudBody);
			OkHttpClient client = new OkHttpClient();
			MediaType mediaType = MediaType.parse(Constantes.APPLICATION_JSON);
			RequestBody body = RequestBody.create(mediaType, solicitudJson);
			Request request = new Request.Builder().url(indicadorDAO.obtenerPropiedad(Constantes.CONF_URL_SERVICIO_AUTENTICAR)).post(body)
					.addHeader("codusuario", coddUsuario).addHeader("token", token)
					.addHeader("content-type", Constantes.APPLICATION_JSON).addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "b8718ee9-f151-8a51-6fa7-c3d492b1c35f").build();

			try {
				Response response = client.newCall(request).execute();
				br = new BufferedReader(new InputStreamReader(response.body().byteStream()));
				respuestaLogueo = (RespuestaLogueo) transformed.extraer(br);
				if(respuestaLogueo.getConfiguracionList().isEmpty()){
					respuestaLogueo = error();
				}else{
					respuestaLogueo.getUsuario().setContrasena(solicitudBody.getContrasena());
				}
			} catch (IOException e) {
				IndicadorException ie = IndicadorException
						.getInstanciaDesdeMensajeOrigen(CodigoEstatus.RECURSO_NO_ENCONTRADO, e.getMessage(), e);
				ie.getTraza().getErrors().add(e.getMessage());
				throw ie;
			}
		} catch (Exception e) {
			respuestaLogueo = error();
		}
		return respuestaLogueo;
	}
	
	/**
	 * Se formatea la respuesta por si existe algun error en el servicio se realice el mapeo
	 * en el front
	 * @return
	 */
	public RespuestaLogueo error(){
		RespuestaLogueo respuestaLogueo = new RespuestaLogueo();
		List<Opcion> opcionList = new ArrayList<Opcion>();
		List<Proceso> procesoList = new ArrayList<Proceso>();
		List<Integer> perfilList = new ArrayList<Integer>();
		List<Configuracion> configuracionList = new ArrayList<Configuracion>();
		Negocio negocio = new Negocio();
		respuestaLogueo.setEstadoTrama(1);
		respuestaLogueo.setOpcionList(opcionList);
		respuestaLogueo.setPerfilList(perfilList);
		respuestaLogueo.setProcesoList(procesoList);
		respuestaLogueo.setConfiguracionList(configuracionList);
		respuestaLogueo.setNegocio(negocio);
		respuestaLogueo.setIngreso(false);			
		respuestaLogueo.setMensaje(Constantes.CONF_MENSAJE_ERROR_USUARIO);
		return respuestaLogueo;
	}

	/**
	 * Metodo para realizar las pruebas localmente sin llamar al servicio
	 * @param indicadorDAO
	 * @param solicitudBody
	 * @param coddUsuario
	 * @param token
	 * @return
	 * @throws IndicadorException
	 */
	public RespuestaLogueo autenticarMock(IIndicadorDAO indicadorDAO, SolicitudLogueo solicitudBody, String coddUsuario, String token)
			throws IndicadorException {
		RespuestaLogueo respuestaLogueo = new RespuestaLogueo();
		Usuario usuario = new Usuario();
		List<Opcion> opcionList = new ArrayList<Opcion>();
		List<HijoOpcion> hijoOpcionList = new ArrayList<HijoOpcion>();
		List<Proceso> procesoList = new ArrayList<Proceso>();
		Proceso proceso = new Proceso();
		Configuracion configuracion = new Configuracion();
		List<Integer> perfilList = new ArrayList<Integer>();
		List<Configuracion> configuracionList = new ArrayList<Configuracion>();
		Negocio negocio = new Negocio();
		respuestaLogueo.setEstadoTrama(1);
		if (solicitudBody.getContrasena().equalsIgnoreCase("noprod")) {
			
			usuario.setIdUsuario(1);
			usuario.setApellido("CRUZ");
			usuario.setNombre("ANTONIO");
			usuario.setCorreo("codisa-naf@divemotor.com.pe");
			usuario.setContrasena("noprod");
			usuario.setLoginUsuario("ACRUZ");
			usuario.setUserAccessToken("7TMxg0gAIJ0=");
			usuario.setPais("001");
			respuestaLogueo.setUsuario(usuario);
			
			Opcion opcion = new Opcion();
			opcion.setEsPadre(true);
			opcion.setEsPadreString("S");
			opcion.setNombre("Venta de vehículo");
			opcion.setUrl("javascript:;");
			opcion.setSimbolo("fa fa-automobile");
			opcion.setNroOrden(4);
			
			HijoOpcion hijoOpcion = new HijoOpcion();
			hijoOpcion.setEsPadre(true);
			hijoOpcion.setEsPadreString("S");
			hijoOpcion.setNombre("Venta de vehículo");
			hijoOpcion.setUrl("javascript:;");
			hijoOpcion.setIdOpcionPadre(800);
			// hijoOpcion.setSimbolo("fa fa-automobile");
			hijoOpcion.setNroOrden(1);

			hijoOpcionList.add(hijoOpcion);
			opcion.setHijoOpcionList(hijoOpcionList);

			opcionList.add(opcion);
			
			
			proceso.setIdProceso(5);
			proceso.setNombre("EDITAR TEXTOS COMERCIALES");
			proceso.setEsRestrictivo(false);
			proceso.setEsRestrictivoString("N");
			procesoList.add(proceso);

			
			configuracion.setCodigo("1");
			configuracion.setDescripcion("Moneda");
			configuracion.setValor("0,0.00");
			configuracion.setTipo("1");
			configuracionList.add(configuracion);

			
			negocio.setNoCia("06");
			negocio.setNombreCia("DIVEMOTOR S.A");
			negocio.setCodAreaVta("002");
			negocio.setDesAreaVta("AUTOS MB");
			negocio.setNombreFilial("ARAMBURU");
			negocio.setCodFamilia(1);
			negocio.setDesFamilia("AUTOMOVILES");
			negocio.setCodMarca("01");
			negocio.setNombreMarca("MERCEDES-BENZ");
			negocio.setIndComercial("N");
			negocio.setIndComercialBoolean(false);
			negocio.setUsuarioSid("ACRUZ");

			
			perfilList.add(369323);
			perfilList.add(5);
			perfilList.add(8);

			respuestaLogueo.setOpcionList(opcionList);
			respuestaLogueo.setPerfilList(perfilList);
			respuestaLogueo.setProcesoList(procesoList);
			respuestaLogueo.setConfiguracionList(configuracionList);
			respuestaLogueo.setNegocio(negocio);
			respuestaLogueo.setIngreso(true);
		} else {
			respuestaLogueo.setOpcionList(opcionList);
			respuestaLogueo.setPerfilList(perfilList);
			respuestaLogueo.setProcesoList(procesoList);
			respuestaLogueo.setConfiguracionList(configuracionList);
			respuestaLogueo.setNegocio(negocio);
			respuestaLogueo.setIngreso(false);			
			respuestaLogueo.setMensaje("El usuario y/o la contraseña son incorrecto");
		}
		return respuestaLogueo;
	}

	/**
	 * Metodo para realizar las pruebas localmente sin llamar al servicio
	 * @param indicadorDAO
	 * @param usuarioDAO 
	 * @param solicitudBody
	 * @param loginUsuario
	 * @param obtenerPropiedad
	 * @return
	 */
	public Perfil perfilMock(IIndicadorDAO indicadorDAO, IUsuarioDAO usuarioDAO, SolicitudLogueo solicitudBody, String loginUsuario,
			String obtenerPropiedad) {
		Perfil perfil = new Perfil();
		perfil.setNombre("ANTONIO CRUZ");
		perfil.setCargo("Genete de Vantas");
		perfil.setTelefono("956 847 969");
		perfil.setCentro("Javier Prado");
		try {
			perfil.setImgLink(StringUtils.join(indicadorDAO.obtenerPropiedad(Constantes.CONF_URL_IMAGE_MECANICOS), "10000017", Constantes.CONF_EXTENSION_IMAGE_MECANICOS,
					Constantes.CONF_PRETOKEN_IMAGE_MECANICOS, indicadorDAO.obtenerPropiedad(Constantes.CONF_TOKEN_IMAGE_MECANICOS)));
		} catch (IndicadorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Taller> talleres = new ArrayList<Taller>();
		Taller t0 = new Taller();
		t0.setId("0");
		t0.setIdCentro("00000001");
		t0.setNombre("J.P. Sede Central");
		Taller t1 = new Taller();
		t1.setId("0");
		t1.setIdCentro("00000001");
		t1.setNombre("J.P. Sede Central");
		talleres.add(t0);
		talleres.add(t1);
		perfil.setTalleres(talleres);
		return perfil;
	}

	public Perfil perfil(IIndicadorDAO indicadorDAO, IUsuarioDAO usuarioDAO, String nombre,
			String loginUsuario, String obtenerPropiedad) throws IndicadorException {
		List<UsuarioTaller> listaUsuarioTaller = null;
		List<Taller> listaTaller = null;
		Perfil perfil = null;
		try {
			listaUsuarioTaller = usuarioDAO.obtenerUsuariosScript(nombre, Integer.parseInt(indicadorDAO.obtenerPropiedad(Constantes.CONF_ID_SISTEMA)));
			if(!listaUsuarioTaller.isEmpty()){
				for (UsuarioTaller usuarioTaller : listaUsuarioTaller) {
					listaTaller = indicadorDAO.obtenerTalleresUsuario(usuarioTaller.getIdUsuario());
					usuarioTaller.setTalleres(listaTaller);
					perfil = new Perfil();
					perfil.setNombre(usuarioTaller.getNombre());
					perfil.setCargo(usuarioTaller.getLogin());
					perfil.setTelefono(usuarioTaller.getCorreo());
					perfil.setTalleres(listaTaller);
					perfil.setCentro(listaTaller.get(0).getIdCentro());
					try {
						perfil.setImgLink(StringUtils.join(indicadorDAO.obtenerPropiedad(Constantes.CONF_URL_IMAGE_MECANICOS), "10000017", Constantes.CONF_EXTENSION_IMAGE_MECANICOS,
								Constantes.CONF_PRETOKEN_IMAGE_MECANICOS, indicadorDAO.obtenerPropiedad(Constantes.CONF_TOKEN_IMAGE_MECANICOS)));
					} catch (IndicadorException e) {
						IndicadorException ie = IndicadorException
								.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_PROVEEDOR_PERISTENCIA, e.getMessage(), e);
						ie.getTraza().getErrors().add(e.getMessage());
						throw ie;
					}
				}						
			}
		} catch (IndicadorException e) {
			IndicadorException ie = IndicadorException
					.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_PROVEEDOR_PERISTENCIA, e.getMessage(), e);
			ie.getTraza().getErrors().add(e.getMessage());
			throw ie;
		}
		return perfil;
	}
}
