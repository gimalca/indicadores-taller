package pe.com.divemotor.indicadores.api.v1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.com.divemotor.indicadores.dominio.Taller;
import pe.com.divemotor.indicadores.dominio.UsuarioTaller;
import pe.com.divemotor.indicadores.dominio.usuario.Perfil;
import pe.com.divemotor.indicadores.dominio.usuario.SolicitudLogueo;
import pe.com.divemotor.indicadores.excepcion.IndicadorException;
import pe.com.divemotor.indicadores.persistencia.IIndicadorDAO;
import pe.com.divemotor.indicadores.persistencia.IUsuarioDAO;
import pe.com.divemotor.indicadores.presentacion.Respuesta;
import pe.com.divemotor.indicadores.presentacion.RespuestaLogueo;
import pe.com.divemotor.indicadores.presentacion.json.IJsonTransformer;
import pe.com.divemotor.indicadores.puente.UsuarioCliente;
import pe.com.divemotor.indicadores.util.CodigoEstatus;
import pe.com.divemotor.indicadores.util.Constantes;

/**
 * 
 * Clase de los endpoints de Usuario
 *
 */
@RestController
public class UsuarioController {

	@Autowired
	private IIndicadorDAO indicadorDAO;
	
	@Autowired
	private IUsuarioDAO usuarioDAO;

	@Autowired
	private IJsonTransformer jsonTransformer;

	@Autowired
	private UsuarioCliente usuarioCliente;

	/**
	 * Endpoint tipo POST que valida con un servicio externo las credenciales del usuario
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @param jsonSolicitud
	 */
	@RequestMapping(value = "/autenticar", method = RequestMethod.POST, consumes = Constantes.APPLICATION_JSON, produces = Constantes.APPLICATION_JSON)
	public void autenticar(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			@RequestBody String jsonSolicitud) {
		Respuesta<RespuestaLogueo> respuesta = null;
		RespuestaLogueo respuestaLogueo = null;
		String jsonSalida = "";
		try {
			SolicitudLogueo solicitudBody = (SolicitudLogueo) jsonTransformer.fromJson(jsonSolicitud,
					SolicitudLogueo.class);
			solicitudBody.validar();
			respuestaLogueo = usuarioCliente.autenticar(indicadorDAO, solicitudBody,
					solicitudBody.getLoginUsuario(), indicadorDAO.obtenerPropiedad(Constantes.TOKEN_SERVICIO_USUARIO));
			if (respuestaLogueo != null) {
				respuesta = new Respuesta<RespuestaLogueo>(CodigoEstatus.OK, respuestaLogueo);
			} else {
				respuesta = new Respuesta<RespuestaLogueo>(CodigoEstatus.OK, respuestaLogueo);
			}
			jsonSalida = jsonTransformer.toJson(respuesta);

			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			httpServletResponse.getWriter().println(jsonSalida);

		} catch (IndicadorException ex) {
			respuestaLogueo = usuarioCliente.error();
			respuesta = new Respuesta<RespuestaLogueo>(CodigoEstatus.ERROR_EN_SOLICITUD, respuestaLogueo);
			jsonSalida = jsonTransformer.toJson(respuesta);

			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			try {
				httpServletResponse.getWriter().println(jsonSalida);
			} catch (IOException ex1) {
				Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex1);
			}

		} catch (Exception ex) {
			httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			try {
				ex.printStackTrace(httpServletResponse.getWriter());
			} catch (IOException ex1) {
				Logger.getLogger(IndicadorController.class.getName()).log(Level.SEVERE, null, ex1);
			}
		}
	}
	
	/**
	 * Endpoint tipo POST que retorna el perfil del usuario conectado
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @param jsonSolicitud
	 */
	@RequestMapping(value = "/perfil", method = RequestMethod.POST, consumes = Constantes.APPLICATION_JSON, produces = Constantes.APPLICATION_JSON)
	public void perfil(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			@RequestBody String jsonSolicitud) {
		Respuesta<Perfil> respuesta = null;
		Perfil perfil = null;
		RespuestaLogueo respuestaLogueo = null;
		String jsonSalida = "";
		try {
			SolicitudLogueo solicitudBody = (SolicitudLogueo) jsonTransformer.fromJson(jsonSolicitud,
					SolicitudLogueo.class);
			solicitudBody.validar();
			respuestaLogueo = usuarioCliente.autenticar(indicadorDAO, solicitudBody,
					solicitudBody.getLoginUsuario(), indicadorDAO.obtenerPropiedad(Constantes.TOKEN_SERVICIO_USUARIO));
			if (respuestaLogueo != null) {
				perfil = usuarioCliente.perfil(indicadorDAO, usuarioDAO, respuestaLogueo.getUsuario().getNombre(), solicitudBody.getLoginUsuario(),
						indicadorDAO.obtenerPropiedad(Constantes.TOKEN_SERVICIO_USUARIO));
				if (perfil != null) {
					respuesta = new Respuesta<Perfil>(CodigoEstatus.OK, perfil);
				} else {
					respuesta = new Respuesta<Perfil>(CodigoEstatus.OK, perfil);
				}
			}			
			jsonSalida = jsonTransformer.toJson(respuesta);

			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			httpServletResponse.getWriter().println(jsonSalida);

		} catch (IndicadorException ex) {
			respuesta = new Respuesta<Perfil>(CodigoEstatus.ERROR_EN_SOLICITUD, perfil);
			jsonSalida = jsonTransformer.toJson(respuesta);

			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			try {
				httpServletResponse.getWriter().println(jsonSalida);
			} catch (IOException ex1) {
				Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex1);
			}

		} catch (Exception ex) {
			httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			try {
				ex.printStackTrace(httpServletResponse.getWriter());
			} catch (IOException ex1) {
				Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex1);
			}
		}
	}
	
	/**
	 * Endpoint tipo GET que retorna las lista de usuarios en la busqueda
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @param search
	 */
	@RequestMapping(value = "/usuarios", method = RequestMethod.GET, produces = Constantes.APPLICATION_JSON)
	public void usuarios(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			@RequestParam(value="search", required=false) String search) {
		Logger.getLogger(UsuarioController.class.getName()).log(Level.INFO, "{INICIO} usuarios");
		Respuesta<List<UsuarioTaller>> respuesta = null;
		List<UsuarioTaller> listaUsuarioTaller = null;
		List<Taller> listaTaller = null;
		String jsonSalida = "";
		try {
			listaUsuarioTaller = usuarioDAO.obtenerUsuariosScript(StringUtils.join(Constantes.PORCENTAJE, search, Constantes.PORCENTAJE), Integer.parseInt(indicadorDAO.obtenerPropiedad(Constantes.CONF_ID_SISTEMA)));
			if (!listaUsuarioTaller.isEmpty()) {
				listaTaller = new ArrayList<Taller>();
				for (UsuarioTaller usuario : listaUsuarioTaller) {
					listaTaller = indicadorDAO.obtenerTalleresUsuario(usuario.getIdUsuario());
					usuario.setTalleres(listaTaller);
				}
				respuesta = new Respuesta<List<UsuarioTaller>>(CodigoEstatus.OK, listaUsuarioTaller);
			} else {
				respuesta = new Respuesta<List<UsuarioTaller>>(CodigoEstatus.OK, listaUsuarioTaller);
			}
			jsonSalida = jsonTransformer.toJson(respuesta);

			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			httpServletResponse.getWriter().println(jsonSalida);

		} catch (IndicadorException ex) {
			respuesta = new Respuesta<List<UsuarioTaller>>(ex);
			jsonSalida = jsonTransformer.toJson(respuesta);

			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			try {
				httpServletResponse.getWriter().println(jsonSalida);
			} catch (IOException ex1) {
				Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex1);
			}

		} catch (Exception ex) {
			IndicadorException ie = IndicadorException
					.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_INTERNO_SERVIDOR, ex.getMessage(), ex);
			ie.getTraza().getErrors().add(ex.getMessage());
			respuesta = new Respuesta<List<UsuarioTaller>>(ie);
			jsonSalida = jsonTransformer.toJson(respuesta);
			httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			httpServletResponse.setContentType(Constantes.CONTENT_TYPE);
			try {
				httpServletResponse.getWriter().println(jsonSalida);
				ex.printStackTrace(httpServletResponse.getWriter());
			} catch (IOException ex1) {
				Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex1);
			}
		}
		Logger.getLogger(UsuarioController.class.getName()).log(Level.INFO, "{FIN} usuarios");
	}
}
