package pe.com.divemotor.indicadores.file.implementacion;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.StringUtils;

import pe.com.divemotor.indicadores.excepcion.IndicadorException;
import pe.com.divemotor.indicadores.persistencia.IIndicadorDAO;
import pe.com.divemotor.indicadores.util.CodigoEstatus;
import pe.com.divemotor.indicadores.util.Constantes;

/**
 * 
 * Clase que da formato a entreda de la lecturas de los csv
 *
 */
public class FormatearHelper {

	public String formatoColor(String color, IIndicadorDAO indicadorDAO) throws IndicadorException {
		String colorPropiedad = StringUtils.join(Constantes.CONF_COLOR, color.trim());
		try {
			colorPropiedad = indicadorDAO.obtenerPropiedad(colorPropiedad);
		} catch (IndicadorException e) {
			throw IndicadorException.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_PROVEEDOR_PERISTENCIA,
					e.getMessage(), e);
		}
		return colorPropiedad;
	}

	public String formatoMoneda(String codigo, IIndicadorDAO indicadorDAO) throws IndicadorException {
		String simbolo = StringUtils.join(Constantes.CONF_MONEDA, codigo.trim());
		try {
			simbolo = indicadorDAO.obtenerPropiedad(simbolo);
		} catch (IndicadorException e) {
			throw IndicadorException.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_PROVEEDOR_PERISTENCIA,
					e.getMessage(), e);
		}
		return simbolo;
	}

	public String formatoNombre(String archivo) {
		String nombre = "";
		Calendar fecha = new GregorianCalendar();
		int anio = fecha.get(Calendar.YEAR);
		int mes = fecha.get(Calendar.MONTH) + 1;
		int dia = fecha.get(Calendar.DAY_OF_MONTH);
		nombre = archivo + anio + String.format("%02d", mes) + String.format("%02d", dia) + Constantes.EXTENSION;
		return nombre;
	}

	public String formatoImgLink(String codigoMecanico, IIndicadorDAO indicadorDAO) throws IndicadorException{
		String imgLink = "";
		try {
			imgLink = StringUtils.join(indicadorDAO.obtenerPropiedad(Constantes.CONF_URL_IMAGE_MECANICOS), codigoMecanico, Constantes.CONF_EXTENSION_IMAGE_MECANICOS,
					Constantes.CONF_PRETOKEN_IMAGE_MECANICOS, indicadorDAO.obtenerPropiedad(Constantes.CONF_TOKEN_IMAGE_MECANICOS));
		} catch (IndicadorException e) {
			throw IndicadorException.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_PROVEEDOR_PERISTENCIA,
					e.getMessage(), e);
		}
		return imgLink;
	}
}
