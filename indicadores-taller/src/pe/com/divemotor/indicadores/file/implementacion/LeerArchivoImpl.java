package pe.com.divemotor.indicadores.file.implementacion;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import com.opencsv.CSVReader;

import pe.com.divemotor.indicadores.dominio.Factura;
import pe.com.divemotor.indicadores.dominio.Taller;
import pe.com.divemotor.indicadores.excepcion.IndicadorException;
import pe.com.divemotor.indicadores.file.ILeerArchivo;
import pe.com.divemotor.indicadores.persistencia.IIndicadorDAO;
import pe.com.divemotor.indicadores.util.CodigoEstatus;
import pe.com.divemotor.indicadores.util.Constantes;

/**
 * 
 * implementacion de leer archivo csv de facturas
 *
 */
public class LeerArchivoImpl implements ILeerArchivo<Factura> {

	@Override
	public synchronized List<Factura> leer(String archivo, IIndicadorDAO indicadorDAO, String codigo, int idUsuario) throws IndicadorException, IOException {
		Logger.getLogger(LeerArchivoImpl.class.getName()).log(Level.INFO, "{INICIO} leer " + archivo);
		String[] registro = null;
		Factura factura = null;
		List<Factura> listaFacturas = null;
		CSVReader reader = null;
		FormatearHelper fh = new FormatearHelper();
		List<Taller> listaTallerUsuario = indicadorDAO.obtenerTalleresUsuario(idUsuario);
		try {
			listaFacturas = new ArrayList<Factura>();
			reader = new CSVReader(new FileReader(fh.formatoNombre(archivo)), ';');
			while ((registro = reader.readNext()) != null) {
				factura = new Factura();
				factura.setCodigoCentro(registro[0].trim());
				factura.setCodigoTaller(registro[1].trim());
				factura.setMes(Integer.parseInt(registro[2].trim()));
				factura.setAnio(Integer.parseInt(registro[3].trim()));
				factura.setCodigoMoneda(fh.formatoMoneda(registro[4], indicadorDAO));
				factura.setDescripcionCentro(registro[5].trim());
				factura.setDescripcionTaller(registro[6].trim());
				factura.setMonto(StringUtils.substringBefore(registro[7].trim(),"."));
				factura.setPorcentajeMeta(new BigDecimal(StringUtils.replace(registro[8].trim(), ",", "")));
				factura.setCodigoColor(fh.formatoColor(registro[9], indicadorDAO));
				factura.setOrdenesAbiertas(Integer.parseInt(registro[10].trim()));
				factura.setPorcentajeProductividad(new BigDecimal(StringUtils.replace(registro[11].trim(), ",", "")));
				factura.setCodigoColorProductividad(fh.formatoColor(registro[12], indicadorDAO));
				factura.setPorcentajeEficiencia(new BigDecimal(StringUtils.replace(registro[13].trim(), ",", "")));
				factura.setCodigoColorEficiencia(fh.formatoColor(registro[14], indicadorDAO));
				factura.setPorcentajeEfectividad(new BigDecimal(StringUtils.replace(registro[15].trim(), ",", "")));
				factura.setCodigoColorEfectividad(fh.formatoColor(registro[16], indicadorDAO));
				factura.setMontoPendienteFacturar(StringUtils.substringBefore(registro[17].trim(),"."));
				for (Taller taller : listaTallerUsuario) {
					if(taller.getId().equals(factura.getCodigoTaller())){
						listaFacturas.add(factura);
					}
				}
				if(factura != null){
					Taller taller = new Taller();
					taller.setId(factura.getCodigoTaller());
					taller.setIdCentro(factura.getCodigoCentro());
					taller.setNombre(factura.getDescripcionTaller());
					indicadorDAO.agregarTaller(taller);
				}
			}
			reader.close();
		} catch (Exception e) {
			throw IndicadorException.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_LECTURA_INTERNA_SERVIDOR,
					e.getMessage(), e);
		}
		Logger.getLogger(LeerArchivoImpl.class.getName()).log(Level.INFO, "{FIN} leer ");
		return listaFacturas;
	}

	@Override
	public synchronized List<Factura> leerFTP(String archivo, IIndicadorDAO indicadorDAO, String codigo, int idUsuario) throws IndicadorException, IOException {
		Logger.getLogger(LeerArchivoImpl.class.getName()).log(Level.INFO, "{INICIO} leerFTP " + archivo);
		String[] registro = null;
		Factura factura = null;
		List<Factura> listaFacturas = null;
		CSVReader reader = null;
		FormatearHelper fh = new FormatearHelper();
		String server = indicadorDAO.obtenerPropiedad(Constantes.CONF_FTP_SERVIDOR);
		String usuario = indicadorDAO.obtenerPropiedad(Constantes.CONF_FTP_USUARIO);
		String password = indicadorDAO.obtenerPropiedad(Constantes.CONF_FTP_PASSWORD);
		int respuesta = 0;
		FTPClient ftp = new FTPClient();
		List<Taller> listaTallerUsuario = indicadorDAO.obtenerTalleresUsuario(idUsuario);
		try {
			ftp.connect(server);
			ftp.login(usuario, password);
			ftp.enterLocalPassiveMode();
			respuesta = ftp.getReplyCode();
			if (FTPReply.isPositiveCompletion(respuesta)) {
				InputStream io = ftp.retrieveFileStream(fh.formatoNombre(archivo));
				reader = new CSVReader(new InputStreamReader(io), ';');
				listaFacturas = new ArrayList<Factura>();
				while ((registro = reader.readNext()) != null) {
					factura = new Factura();
					factura.setCodigoCentro(registro[0].trim());
					factura.setCodigoTaller(registro[1].trim());
					factura.setMes(Integer.parseInt(registro[2].trim()));
					factura.setAnio(Integer.parseInt(registro[3].trim()));
					factura.setCodigoMoneda(fh.formatoMoneda(registro[4], indicadorDAO));
					factura.setDescripcionCentro(registro[5].trim());
					factura.setDescripcionTaller(registro[6].trim());
					factura.setMonto(StringUtils.substringBefore(registro[7].trim(),"."));
					factura.setPorcentajeMeta(new BigDecimal(StringUtils.replace(registro[8].trim(), ",", "")));
					factura.setCodigoColor(fh.formatoColor(registro[9], indicadorDAO));
					factura.setOrdenesAbiertas(Integer.parseInt(registro[10].trim()));
					factura.setPorcentajeProductividad(
							new BigDecimal(StringUtils.replace(registro[11].trim(), ",", "")));
					factura.setCodigoColorProductividad(fh.formatoColor(registro[12], indicadorDAO));
					factura.setPorcentajeEficiencia(new BigDecimal(StringUtils.replace(registro[13].trim(), ",", "")));
					factura.setCodigoColorEficiencia(fh.formatoColor(registro[14], indicadorDAO));
					factura.setPorcentajeEfectividad(new BigDecimal(StringUtils.replace(registro[15].trim(), ",", "")));
					factura.setCodigoColorEfectividad(fh.formatoColor(registro[16], indicadorDAO));
					factura.setMontoPendienteFacturar(StringUtils.substringBefore(registro[17].trim(),"."));
					for (Taller taller : listaTallerUsuario) {
						if(taller.getId().equals(factura.getCodigoTaller())){
							listaFacturas.add(factura);
						}
					}										
					if(factura != null){
						Taller taller = new Taller();
						taller.setId(factura.getCodigoTaller());
						taller.setIdCentro(factura.getCodigoCentro());
						taller.setNombre(factura.getDescripcionTaller());
						indicadorDAO.agregarTaller(taller);
					}
				}
				reader.close();
			}
			ftp.logout();
			ftp.disconnect();
		} catch (SocketException e) {
			throw IndicadorException.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_LECTURA_INTERNA_SERVIDOR,
					e.getMessage(), e);
		} catch (Exception e) {
			throw IndicadorException.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_LECTURA_INTERNA_SERVIDOR,
					e.getMessage(), e);
		}
		Logger.getLogger(LeerArchivoImpl.class.getName()).log(Level.INFO, "{FIN} leerFTP ");
		return listaFacturas;
	}

}
