package pe.com.divemotor.indicadores.file.implementacion;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import com.opencsv.CSVReader;

import pe.com.divemotor.indicadores.dominio.Tecnico;
import pe.com.divemotor.indicadores.excepcion.IndicadorException;
import pe.com.divemotor.indicadores.file.ILeerArchivo;
import pe.com.divemotor.indicadores.persistencia.IIndicadorDAO;
import pe.com.divemotor.indicadores.util.CodigoEstatus;
import pe.com.divemotor.indicadores.util.Constantes;

/**
 * 
 * implementacion de leer archivo csv de tecnicos
 *
 */
public class LeerArchivoImplTecnico implements ILeerArchivo<Tecnico> {

	@Override
	public synchronized List<Tecnico> leer(String archivo, IIndicadorDAO indicadorDAO, String codigoTaller, int idusuario)
			throws IndicadorException, IOException {
//		Logger.getLogger(LeerArchivoImpl.class.getName()).log(Level.INFO, "{INICIO} leer " + archivo);
		String[] registro = null;
		Tecnico tecnico = null;
		List<Tecnico> listaTecnicos = null;
		CSVReader reader = null;
		FormatearHelper fh = new FormatearHelper();
		try {
			listaTecnicos = new ArrayList<Tecnico>();
			reader = new CSVReader(new FileReader(fh.formatoNombre(archivo)), ';');
			while ((registro = reader.readNext()) != null) {
				tecnico = new Tecnico();
				if (codigoTaller.equals(registro[1].trim())) {
					tecnico.setCodigoCentro(registro[0].trim());
					tecnico.setCodigoTaller(registro[1].trim());
					tecnico.setMes(Integer.parseInt(registro[2].trim()));
					tecnico.setAnio(Integer.parseInt(registro[3].trim()));
					tecnico.setCodigoMecanico(registro[4].trim());
					tecnico.setDescripcionCentro(registro[5].trim());
					tecnico.setDescripcionTaller(registro[6].trim());
					tecnico.setNombreMecanico(registro[7].trim());
					tecnico.setHrsPres(StringUtils.substring(registro[8].trim(),0,registro[8].trim().length()-1));
					tecnico.setHrsReal(StringUtils.substring(registro[9].trim(),0,registro[9].trim().length()-1));
					tecnico.setHrsNot(StringUtils.substring(registro[10].trim(),0,registro[10].trim().length()-1));
					tecnico.setPorcentajeProductividad(
							new BigDecimal(StringUtils.replace(registro[11].trim(), ",", "")));
					tecnico.setCodigoColorProductividad(fh.formatoColor(registro[12], indicadorDAO));
					tecnico.setPorcentajeEficiencia(new BigDecimal(StringUtils.replace(registro[13].trim(), ",", "")));
					tecnico.setCodigoColorEficiencia(fh.formatoColor(registro[14], indicadorDAO));
					tecnico.setPorcentajeEfectividad(new BigDecimal(StringUtils.replace(registro[15].trim(), ",", "")));
					tecnico.setCodigoColorEfectividad(fh.formatoColor(registro[16], indicadorDAO));
					tecnico.setImgLink(fh.formatoImgLink(tecnico.getCodigoMecanico(), indicadorDAO));
					listaTecnicos.add(tecnico);
				}
			}
			reader.close();
		} catch (Exception e) {
			throw IndicadorException.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_LECTURA_INTERNA_SERVIDOR,
					e.getMessage(), e);
		}
//		Logger.getLogger(LeerArchivoImpl.class.getName()).log(Level.INFO, "{FIN} leer ");
		return listaTecnicos;
	}

	@Override
	public synchronized List<Tecnico> leerFTP(String archivo, IIndicadorDAO indicadorDAO, String codigoTaller, int idusario)
			throws IndicadorException, IOException {
//		Logger.getLogger(LeerArchivoImpl.class.getName()).log(Level.INFO, "{INICIO} leerFTP " + archivo);
		String[] registro = null;
		Tecnico tecnico = null;
		List<Tecnico> listaTecnicos = null;
		CSVReader reader = null;
		FormatearHelper fh = new FormatearHelper();
		String server = indicadorDAO.obtenerPropiedad(Constantes.CONF_FTP_SERVIDOR);
		String usuario = indicadorDAO.obtenerPropiedad(Constantes.CONF_FTP_USUARIO);
		String password = indicadorDAO.obtenerPropiedad(Constantes.CONF_FTP_PASSWORD);
		int respuesta = 0;
		FTPClient ftp = new FTPClient();

		try {
			ftp.connect(server);
			ftp.login(usuario, password);
			ftp.enterLocalPassiveMode();
			respuesta = ftp.getReplyCode();
			if (FTPReply.isPositiveCompletion(respuesta)) {
				InputStream io = ftp.retrieveFileStream(fh.formatoNombre(archivo));
				reader = new CSVReader(new InputStreamReader(io), ';');
				listaTecnicos = new ArrayList<Tecnico>();
				while ((registro = reader.readNext()) != null) {
					tecnico = new Tecnico();
					if (codigoTaller.equals(registro[1].trim())) {
						tecnico.setCodigoCentro(registro[0].trim());
						tecnico.setCodigoTaller(registro[1].trim());
						tecnico.setMes(Integer.parseInt(registro[2].trim()));
						tecnico.setAnio(Integer.parseInt(registro[3].trim()));
						tecnico.setCodigoMecanico(registro[4].trim());
						tecnico.setDescripcionCentro(registro[5].trim());
						tecnico.setDescripcionTaller(registro[6].trim());
						tecnico.setNombreMecanico(registro[7].trim());
						tecnico.setHrsPres(StringUtils.substring(registro[8].trim(),0,registro[8].trim().length()-1));
						tecnico.setHrsReal(StringUtils.substring(registro[9].trim(),0,registro[9].trim().length()-1));
						tecnico.setHrsNot(StringUtils.substring(registro[10].trim(),0,registro[10].trim().length()-1));
						tecnico.setPorcentajeProductividad(
								new BigDecimal(StringUtils.replace(registro[11].trim(), ",", "")));
						tecnico.setCodigoColorProductividad(fh.formatoColor(registro[12], indicadorDAO));
						tecnico.setPorcentajeEficiencia(
								new BigDecimal(StringUtils.replace(registro[13].trim(), ",", "")));
						tecnico.setCodigoColorEficiencia(fh.formatoColor(registro[14], indicadorDAO));
						tecnico.setPorcentajeEfectividad(
								new BigDecimal(StringUtils.replace(registro[15].trim(), ",", "")));
						tecnico.setCodigoColorEfectividad(fh.formatoColor(registro[16], indicadorDAO));
						tecnico.setImgLink(fh.formatoImgLink(tecnico.getCodigoMecanico(), indicadorDAO));
						listaTecnicos.add(tecnico);
					}
				}
				reader.close();
			}
			ftp.logout();
			ftp.disconnect();
		} catch (SocketException e) {
			throw IndicadorException.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_LECTURA_INTERNA_SERVIDOR,
					e.getMessage(), e);
		} catch (Exception e) {
			throw IndicadorException.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_LECTURA_INTERNA_SERVIDOR,
					e.getMessage(), e);
		}
//		Logger.getLogger(LeerArchivoImpl.class.getName()).log(Level.INFO, "{FIN} leerFTP ");
		return listaTecnicos;
	}

}
