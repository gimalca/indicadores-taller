package pe.com.divemotor.indicadores.file.implementacion;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import com.opencsv.CSVReader;

import pe.com.divemotor.indicadores.dominio.Entrega;
import pe.com.divemotor.indicadores.excepcion.IndicadorException;
import pe.com.divemotor.indicadores.file.ILeerArchivo;
import pe.com.divemotor.indicadores.persistencia.IIndicadorDAO;
import pe.com.divemotor.indicadores.util.CodigoEstatus;
import pe.com.divemotor.indicadores.util.Constantes;

/**
 * 
 * implementacion de leer archivo csv de entregas
 *
 */
public class LeerArchivoImplEntrega implements ILeerArchivo<Entrega> {

	@Override
	public synchronized List<Entrega> leer(String archivo, IIndicadorDAO indicadorDAO, String codigoTaller, int idUsuario)
			throws IndicadorException, IOException {
//		Logger.getLogger(LeerArchivoImpl.class.getName()).log(Level.INFO, "{INICIO} leer " + archivo);
		String[] registro = null;
		Entrega entrega = null;
		List<Entrega> listaEntregas = null;
		CSVReader reader = null;
		FormatearHelper fh = new FormatearHelper();
		try {
			listaEntregas = new ArrayList<Entrega>();
			reader = new CSVReader(new FileReader(fh.formatoNombre(archivo)), ';');
			while ((registro = reader.readNext()) != null) {
				entrega = new Entrega();
				if (codigoTaller.equals(registro[1].trim())) {
					entrega.setCodigoCentro(registro[0].trim());
					entrega.setCodigoTaller(registro[1].trim());
					entrega.setMes(Integer.parseInt(registro[2].trim()));
					entrega.setAnio(Integer.parseInt(registro[3].trim()));
					entrega.setCodigoMecanico(registro[4].trim());
					entrega.setCodigoCliente(registro[5].trim());
					entrega.setHora(StringUtils.substringBeforeLast(registro[6].trim(),":"));
					entrega.setPlaca(registro[7].trim());
					entrega.setNumeroOrdenTrabajo(Integer.parseInt(registro[8].trim()));
					entrega.setDescripcionCentro(registro[9].trim());
					entrega.setDescripcionTaller(registro[10].trim());
					entrega.setNombreAsesor(registro[11].trim());
					entrega.setNombreCliente(registro[12].trim());
					entrega.setPorcentajeEntrega(new BigDecimal(StringUtils.replace(registro[13].trim(), ",", "")));
					entrega.setCodigoColorEntrega(fh.formatoColor(registro[12], indicadorDAO));
					listaEntregas.add(entrega);
				}
			}
			reader.close();
		} catch (Exception e) {
			throw IndicadorException.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_LECTURA_INTERNA_SERVIDOR,
					e.getMessage(), e);
		}
//		Logger.getLogger(LeerArchivoImpl.class.getName()).log(Level.INFO, "{FIN} leer ");
		return listaEntregas;
	}

	@Override
	public synchronized List<Entrega> leerFTP(String archivo, IIndicadorDAO indicadorDAO, String codigoTaller, int idUsuario)
			throws IndicadorException, IOException {
//		Logger.getLogger(LeerArchivoImpl.class.getName()).log(Level.INFO, "{INICIO} leerFTP " + archivo);
		String[] registro = null;
		Entrega entrega = null;
		List<Entrega> listaEntregas = null;
		CSVReader reader = null;
		FormatearHelper fh = new FormatearHelper();
		String server = indicadorDAO.obtenerPropiedad(Constantes.CONF_FTP_SERVIDOR);
		String usuario = indicadorDAO.obtenerPropiedad(Constantes.CONF_FTP_USUARIO);
		String password = indicadorDAO.obtenerPropiedad(Constantes.CONF_FTP_PASSWORD);
		int respuesta = 0;
		FTPClient ftp = new FTPClient();

		try {
			ftp.connect(server);
			ftp.login(usuario, password);
			ftp.enterLocalPassiveMode();
			respuesta = ftp.getReplyCode();
			if (FTPReply.isPositiveCompletion(respuesta)) {
				InputStream io = ftp.retrieveFileStream(fh.formatoNombre(archivo));
				reader = new CSVReader(new InputStreamReader(io), ';');
				listaEntregas = new ArrayList<Entrega>();
				while ((registro = reader.readNext()) != null) {
					entrega = new Entrega();
					if (codigoTaller.equals(registro[1].trim())) {
						entrega.setCodigoCentro(registro[0].trim());
						entrega.setCodigoTaller(registro[1].trim());
						entrega.setMes(Integer.parseInt(registro[2].trim()));
						entrega.setAnio(Integer.parseInt(registro[3].trim()));
						entrega.setCodigoMecanico(registro[4].trim());
						entrega.setCodigoCliente(registro[5].trim());
						entrega.setHora(StringUtils.substringBeforeLast(registro[6].trim(),":"));
						entrega.setPlaca(registro[7].trim());
						entrega.setNumeroOrdenTrabajo(Integer.parseInt(registro[8].trim()));
						entrega.setDescripcionCentro(registro[9].trim());
						entrega.setDescripcionTaller(registro[10].trim());
						entrega.setNombreAsesor(registro[11].trim());
						entrega.setNombreCliente(registro[12].trim());
						entrega.setPorcentajeEntrega(new BigDecimal(StringUtils.replace(registro[13].trim(), ",", "")));
						entrega.setCodigoColorEntrega(fh.formatoColor(registro[12], indicadorDAO));
						listaEntregas.add(entrega);
					}
				}
				reader.close();
			}
			ftp.logout();
			ftp.disconnect();
		} catch (SocketException e) {
			throw IndicadorException.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_LECTURA_INTERNA_SERVIDOR,
					e.getMessage(), e);
		} catch (Exception e) {
			throw IndicadorException.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_LECTURA_INTERNA_SERVIDOR,
					e.getMessage(), e);
		}
//		Logger.getLogger(LeerArchivoImpl.class.getName()).log(Level.INFO, "{FIN} leerFTP ");
		return listaEntregas;
	}

}
