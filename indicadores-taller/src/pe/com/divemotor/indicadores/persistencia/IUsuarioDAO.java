package pe.com.divemotor.indicadores.persistencia;

import java.util.List;

import pe.com.divemotor.indicadores.dominio.UsuarioTaller;
import pe.com.divemotor.indicadores.excepcion.IndicadorException;

/**
 * 
 * Metodos disponibles de acceso a los datos de la base de datos usuarios
 *
 */
public interface IUsuarioDAO {

	public List<UsuarioTaller> obtenerUsuarios(String busqueda, int idSistema) throws IndicadorException;
	
	public List<UsuarioTaller> obtenerUsuariosScript(String busqueda, int idSitema) throws IndicadorException;
	
}
