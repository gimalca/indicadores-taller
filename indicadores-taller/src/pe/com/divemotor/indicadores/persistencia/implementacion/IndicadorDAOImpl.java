package pe.com.divemotor.indicadores.persistencia.implementacion;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import oracle.jdbc.OracleTypes;
import pe.com.divemotor.indicadores.dominio.Menu;
import pe.com.divemotor.indicadores.dominio.Propiedad;
import pe.com.divemotor.indicadores.dominio.SolicitudUsuarioTaller;
import pe.com.divemotor.indicadores.dominio.Taller;
import pe.com.divemotor.indicadores.excepcion.IndicadorException;
import pe.com.divemotor.indicadores.persistencia.IIndicadorDAO;
import pe.com.divemotor.indicadores.util.CodigoEstatus;

/**
 * 
 * Clase de implementacion de DB indicadores
 *
 */
public class IndicadorDAOImpl implements IIndicadorDAO {

	private JdbcTemplate jdbcTemplate;
	private DataSource dataSource;
	private ApplicationContext context;
	private SimpleJdbcCall obtenerPropiedad;
	private SimpleJdbcCall actualizarPropiedad;
	private SimpleJdbcCall actualizarUsuarioTaller;
	private SimpleJdbcCall eliminarUsuarioTaller;
	private SimpleJdbcCall agregarTaller;

	@Override
	public synchronized String obtenerPropiedad(String clave) throws IndicadorException {
		SqlParameterSource in = new MapSqlParameterSource().addValue("p_CLAVE", clave);
		Map<String, Object> out = obtenerPropiedad.execute(in);
		String valor = null;
		if (out.get("P_VALOR") != null) {
			valor = (String) out.get("P_VALOR");
		}
		return valor;
	}

	@Override
	public String updatePropiedad(int id, Propiedad propiedad) throws IndicadorException {
		SqlParameterSource in = new MapSqlParameterSource().addValue("p_ID_PROPIEDAD", id)
				.addValue("p_CLAVE", propiedad.getClave()).addValue("p_VALOR", propiedad.getValor())
				.addValue("p_DESCRIPCION", propiedad.getDescripcion());
		Map<String, Object> out = actualizarPropiedad.execute(in);
		String valor = null;
		if (out.get("P_RESULTADO") != null) {
			valor = (String) out.get("P_RESULTADO");
		}
		return valor;
	}

	@Override
	public List<Propiedad> listar() throws IndicadorException {
		List<Propiedad> listaPropiedades = null;
		String sql = "{CALL LISTAR_PROPIEDADES(?)}";
		CallableStatement callableSt = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
			callableSt = conn.prepareCall(sql);

			callableSt.registerOutParameter(1, OracleTypes.CURSOR);

			callableSt.executeUpdate();
			rs = (ResultSet) callableSt.getObject(1);
			listaPropiedades = new ArrayList<Propiedad>();
			while (rs.next()) {
				Propiedad propiedad = new Propiedad();
				propiedad.setId(rs.getInt("ID_PROPIEDAD"));
				propiedad.setClave(rs.getString("CLAVE"));
				propiedad.setValor(rs.getString("VALOR"));
				propiedad.setDescripcion(rs.getString("DESCRIPCION"));
				listaPropiedades.add(propiedad);
			}
		} catch (SQLException e) {
			IndicadorException ie = IndicadorException
					.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_PROVEEDOR_PERISTENCIA, e.getMessage(), e);
			ie.getTraza().getErrors().add(e.getMessage());
			throw ie;
		} finally {
			try {
				if (!callableSt.isClosed()) {
					callableSt.close();
				}
				if (!rs.isClosed()) {
					rs.close();
				}
				conn.close();
			} catch (SQLException e) {
				IndicadorException ie = IndicadorException
						.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_PROVEEDOR_PERISTENCIA, e.getMessage(), e);
				ie.getTraza().getErrors().add(e.getMessage());
				throw ie;
			}
		}
		return listaPropiedades;
	}

	@Override
	public List<Menu> obtenerMenu(Integer idPerfil) throws IndicadorException {
		List<Menu> listaMenus = null;
		String sql = "{CALL OBTENER_MENU(?,?,?)}";
		CallableStatement callableSt = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
			callableSt = conn.prepareCall(sql);

			callableSt.setInt(1, idPerfil);
			callableSt.setString(2, "1");
			callableSt.registerOutParameter(3, OracleTypes.CURSOR);

			callableSt.executeUpdate();
			rs = (ResultSet) callableSt.getObject(3);
			listaMenus = new ArrayList<Menu>();
			while (rs.next()) {
				Menu menu = new Menu();
				menu.setId(rs.getInt("ID_MENU"));
				menu.setTitle(rs.getString("TITLE"));
				menu.setRouterLink(rs.getString("ROUTER_LINK"));
				menu.setIcon(rs.getString("ICON"));
				menu.setSelected(rs.getString("SELECTED").equals("S") ? true : false);
				menu.setExpanded(rs.getString("EXPANDED").equals("S") ? true : false);
				menu.setOrder(rs.getInt("ORDEN"));
				listaMenus.add(menu);
			}
		} catch (SQLException e) {
			IndicadorException ie = IndicadorException
					.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_PROVEEDOR_PERISTENCIA, e.getMessage(), e);
			ie.getTraza().getErrors().add(e.getMessage());
			throw ie;
		} finally {
			try {
				if (!callableSt.isClosed()) {
					callableSt.close();
				}
				if (!rs.isClosed()) {
					rs.close();
				}
				conn.close();
			} catch (SQLException e) {
				IndicadorException ie = IndicadorException
						.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_PROVEEDOR_PERISTENCIA, e.getMessage(), e);
				ie.getTraza().getErrors().add(e.getMessage());
				throw ie;
			}
		}
		return listaMenus;
	}

	@Override
	public List<Taller> obtenerTalleresUsuario(int idUsuario) throws IndicadorException {
		List<Taller> listaTaller = null;
		String sql = "{CALL CONSULTAR_TALLERES_USUARIOS(?,?)}";
		CallableStatement callableSt = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
			callableSt = conn.prepareCall(sql);

			callableSt.setInt(1, idUsuario);
			callableSt.registerOutParameter(2, OracleTypes.CURSOR);

			callableSt.executeUpdate();
			rs = (ResultSet) callableSt.getObject(2);
			listaTaller = new ArrayList<Taller>();
			while (rs.next()) {
				Taller taller = new Taller();
				taller.setId(rs.getString("ID_TALLER"));
				taller.setNombre(rs.getString("NOMBRE"));
				listaTaller.add(taller);
			}
		} catch (SQLException e) {
			IndicadorException ie = IndicadorException
					.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_PROVEEDOR_PERISTENCIA, e.getMessage(), e);
			ie.getTraza().getErrors().add(e.getMessage());
			throw ie;
		} finally {
			try {
				if (!callableSt.isClosed()) {
					callableSt.close();
				}
				if (!rs.isClosed()) {
					rs.close();
				}
				conn.close();
			} catch (SQLException e) {
				IndicadorException ie = IndicadorException
						.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_PROVEEDOR_PERISTENCIA, e.getMessage(), e);
				ie.getTraza().getErrors().add(e.getMessage());
				throw ie;
			}
		}
		return listaTaller;
	}

	@Override
	public List<Taller> listarTaller() throws IndicadorException {
		List<Taller> listaTalleres = null;
		String sql = "{CALL LISTAR_TALLERES(?)}";
		CallableStatement callableSt = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
			callableSt = conn.prepareCall(sql);

			callableSt.registerOutParameter(1, OracleTypes.CURSOR);

			callableSt.executeUpdate();
			rs = (ResultSet) callableSt.getObject(1);
			listaTalleres = new ArrayList<Taller>();
			while (rs.next()) {
				Taller taller = new Taller();
				taller.setId(rs.getString("ID_TALLER"));
				taller.setIdCentro(rs.getString("ID_CENTRO"));
				taller.setNombre(rs.getString("NOMBRE"));
				listaTalleres.add(taller);
			}
		} catch (SQLException e) {
			IndicadorException ie = IndicadorException
					.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_PROVEEDOR_PERISTENCIA, e.getMessage(), e);
			ie.getTraza().getErrors().add(e.getMessage());
			throw ie;
		} finally {
			try {
				if (!callableSt.isClosed()) {
					callableSt.close();
				}
				if (!rs.isClosed()) {
					rs.close();
				}
				conn.close();
			} catch (SQLException e) {
				IndicadorException ie = IndicadorException
						.getInstanciaDesdeMensajeOrigen(CodigoEstatus.ERROR_PROVEEDOR_PERISTENCIA, e.getMessage(), e);
				ie.getTraza().getErrors().add(e.getMessage());
				throw ie;
			}
		}
		return listaTalleres;
	}

	@Override
	public String updateUsuarioTaller(int id, SolicitudUsuarioTaller usuarioTaller) throws IndicadorException {
		String valor = null;
		SqlParameterSource in = new MapSqlParameterSource().addValue("p_ID_USUARIO", id);
		Map<String, Object> out = eliminarUsuarioTaller.execute(in);
		for (String idTaller : usuarioTaller.getIdsTalleres()) {
			in = new MapSqlParameterSource().addValue("p_ID_USUARIO", id).addValue("p_ID_TALLER", idTaller);
			out = actualizarUsuarioTaller.execute(in);
			if (out.get("P_RESULTADO") != null) {
				valor = (String) out.get("P_RESULTADO");
			}
		}
		return valor;
	}

	@Override
	public String agregarTaller(Taller taller) throws IndicadorException {
		String valor = null;
		SqlParameterSource in = new MapSqlParameterSource().addValue("p_ID_TALLER", taller.getId())
				.addValue("p_ID_CENTRO", taller.getIdCentro()).addValue("p_NOMBRE", taller.getNombre());
		Map<String, Object> out = agregarTaller.execute(in);
		if (out.get("P_RESULTADO") != null) {
			valor = (String) out.get("P_RESULTADO");
		}
		return valor;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.setJdbcTemplate(new JdbcTemplate(dataSource));
		this.obtenerPropiedad = new SimpleJdbcCall(dataSource).withProcedureName("OBTENER_PROPIEDAD");
		this.actualizarPropiedad = new SimpleJdbcCall(dataSource).withProcedureName("ACTUALIZAR_PROPIEDAD");
		this.actualizarUsuarioTaller = new SimpleJdbcCall(dataSource).withProcedureName("ACTUALIZAR_USUARIO_TALLER");
		this.eliminarUsuarioTaller = new SimpleJdbcCall(dataSource).withProcedureName("ELIMINAR_USUARIO_TALLER");
		this.agregarTaller = new SimpleJdbcCall(dataSource).withProcedureName("AGREGAR_TALLERES");
	}

	public IndicadorDAOImpl getIndicadorDAOImpl() {
		context = new ClassPathXmlApplicationContext("jdbc-config.xml");
		return (IndicadorDAOImpl) context.getBean("indicadorDAO");
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
